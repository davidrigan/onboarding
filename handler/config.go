package handler

import (
	"bitbucket.org/finaccelteam/onboarding/broker/subscriber"
)

var (
	CreatePartnerKafkaConfig        subscriber.ConsumerConfig
	CreatePartnerChannelKafkaConfig subscriber.ConsumerConfig
)

func GetCreatePartnerKafkaConfig() *subscriber.ConsumerConfig {
	return &CreatePartnerKafkaConfig
}

func GetCreatePartnerChannelKafkaConfig() *subscriber.ConsumerConfig {
	return &CreatePartnerChannelKafkaConfig
}
