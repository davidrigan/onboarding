package subscriber

import "bitbucket.org/finaccelteam/ms-common-go/broker/kafka"

const (
	standardKafkaPollInterval    = 300                           // could be used as default value
	standardkafkaPartitionOffset = kafka.PartitionOffsetEarliest // could be used as default value
)
