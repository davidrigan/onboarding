// Code generated by Wire. DO NOT EDIT.

//go:generate go run github.com/google/wire/cmd/wire
//go:build !wireinject
// +build !wireinject

package service

import (
	"bitbucket.org/finaccelteam/onboarding/container"
	"bitbucket.org/finaccelteam/onboarding/repository"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"bitbucket.org/finaccelteam/onboarding/service/partnerChannel"
	"context"
	"github.com/google/wire"
)

// Injectors from impl_wire_service.go:

func wireGetPartnerChannelService(ctx context.Context) (partnerChannel.Service, error) {
	queries, err := container.GetNewSQLQueries(ctx)
	if err != nil {
		return nil, err
	}
	partnerChannelRepository := repository.NewPartnerChannelRepository(queries)
	partnerRepository := repository.NewPartnerRepository(queries)
	service := partnerChannel.NewService(partnerChannelRepository, partnerRepository)
	return service, nil
}

func wireGetPartnerService(ctx context.Context) (partner.Service, error) {
	queries, err := container.GetNewSQLQueries(ctx)
	if err != nil {
		return nil, err
	}
	partnerRepository := repository.NewPartnerRepository(queries)
	service := partner.NewService(partnerRepository)
	return service, nil
}

// impl_wire_service.go:

var newServRepo = wire.NewSet(repository.NewPartnerRepository, repository.NewPartnerChannelRepository)
