package controller

import (
	commonController "bitbucket.org/finaccelteam/ms-common-go/controller"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"bitbucket.org/finaccelteam/onboarding/middleware"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
	"bitbucket.org/finaccelteam/onboarding/service/partnerChannel"
	"context"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

// 4
type partnerChannelController struct {
	service     partnerChannel.Service
	tx          sql.Transactional
	idempotency middleware.IdempotencyMiddleWare
	validate    *validator.Validate
}

func (c *partnerChannelController) RegisterRoutes(router *mux.Router) {
	router.Handle("/partnerChannels/{id:[0-9]+}", fhttp.AppHandler(c.getPartnerChannel)).Methods("GET")
	router.Handle("/partnerChannels/{id:[0-9]+}", fhttp.AppHandler(c.updatePartnelChannel)).Methods("PUT")
}

func (c *partnerChannelController) getPartnerChannel(r *http.Request) (*fhttp.Response, error) {
	ctx := r.Context()
	partnerChannelID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeResourcesMissing, err, "getPartnerChannel", http.StatusUnprocessableEntity)
	}

	var resp *response.PartnerChannel
	if err = c.tx.WithTransaction(ctx, func(ctx context.Context) error {
		resp, err = c.service.GetPartnerChannel(r.Context(), partnerChannelID)
		if err != nil {
			return ferror.Wrap(err, "GetPartnerChannel")
		}
		return nil
	}); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return &fhttp.Response{
		Data:       resp,
		StatusCode: http.StatusOK,
	}, nil
}

func (c *partnerChannelController) updatePartnelChannel(r *http.Request) (*fhttp.Response, error) {
	ctx := r.Context()
	partnerChannelID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeResourcesMissing, err, "updatePartnelChannel", http.StatusUnprocessableEntity)
	}
	req := &request.UpdatePartnerChannel{}

	if err = json.NewDecoder(r.Body).Decode(req); err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeInvalidDecode, err, "updatePartnelChannel", http.StatusUnprocessableEntity)
	}
	if err = c.validate.Struct(req); err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeInvalidValidateStruct, err, "updatePartnelChannel", http.StatusBadRequest)
	}

	if err = c.tx.WithTransaction(ctx, func(ctx context.Context) error {
		if err = c.service.UpdatePartnerChannel(ctx, req, partnerChannelID); err != nil {
			return ferror.Wrap(err, "UpdatePartnerChannel")
		}

		return nil
	}); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	logger.Info(ctx, "Successfully update data with partnerChannelID: %v", partnerChannelID)
	return &fhttp.Response{
		Data:       req,
		StatusCode: http.StatusOK,
	}, nil
}

// 3
func NewPartnerChannelController(router *mux.Router, service partnerChannel.Service, tx sql.Transactional, idempotency middleware.IdempotencyMiddleWare) commonController.Controller {
	partnerChannelController := &partnerChannelController{
		service:     service,
		tx:          tx,
		idempotency: idempotency,
		validate:    validator.New(),
	}
	partnerChannelController.RegisterRoutes(router)
	return partnerChannelController
}
