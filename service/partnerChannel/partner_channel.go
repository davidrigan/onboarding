package partnerChannel

import (
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	domainPartner "bitbucket.org/finaccelteam/onboarding/domain/partner"
	domain "bitbucket.org/finaccelteam/onboarding/domain/partnerChannel"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
	"context"
)

//6
type Service interface {
	GetPartnerChannel(ctx context.Context, id int) (*response.PartnerChannel, error)
	CreatePartnerChannel(ctx context.Context, partner *request.PartnerChannel) (int, error)
	UpdatePartnerChannel(ctx context.Context, partnerChannel *request.UpdatePartnerChannel, id int) error
	FindPartnerChannelById(ctx context.Context, partnerID int) (*response.PartnerChannel, error)
}

type service struct {
	repo        domain.Repository
	repoPartner domainPartner.Repository
}

func (s *service) GetPartnerChannel(ctx context.Context, id int) (*response.PartnerChannel, error) {
	partnerChannelID, err := s.repo.FindByID(ctx, id)
	if err != nil {
		return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "GetPartnerChannel FindByID repo")
	}
	if partnerChannelID == nil {
		return nil, errors.ErrKindNotFound(errors.ErrCodePartnerChannelNotFound, "GetPartnerChannel Check")
	}
	resp := s.mapDomainToResponse(partnerChannelID)
	return &resp, nil
}

func (s *service) CreatePartnerChannel(ctx context.Context, partner *request.PartnerChannel) (int, error) {
	if partner == nil {
		return -1, errors.ErrKindUnprocessable(errors.ErrCodeBadRequest, "CreatePartner partner request")
	}
	partnerID := partner.PartnerID
	_, err := s.repoPartner.FindByID(ctx, partnerID)
	if err != nil {
		return -1, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "CreatePartnerChannel FindByID repo")
	}

	logger.Info(ctx, "Data for partnerID %v found!", partnerID)
	req := s.mapRequestToDomain(partner)
	id, err := s.repo.Save(ctx, &req)
	if err != nil {
		return 0, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "CreatePartnerChannel Save repo")
	}
	return id, nil
}

func (s *service) UpdatePartnerChannel(ctx context.Context, partnerChannel *request.UpdatePartnerChannel, id int) error {
	if partnerChannel == nil {
		return errors.ErrKindUnprocessable(errors.ErrCodeBadRequest, "CreatePartner partner request")
	}
	// we check first if data is exist
	partnerChannelCheck, err := s.repo.FindByID(ctx, id)
	if err != nil {
		return errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "UpdatePartnerChannel FindByID repo")
	}
	// if data not exists
	if partnerChannelCheck == nil {
		return errors.ErrKindInternalServerError(errors.ErrCodePartnerChannelNotFound, "UpdatePartnerChannel Check")
	}

	// check value if nill
	if partnerChannel.Name != nil {
		partnerChannelCheck.Name = *partnerChannel.Name
	}
	if partnerChannel.DisplayName != nil {
		partnerChannelCheck.DisplayName = *partnerChannel.DisplayName
	}
	if partnerChannel.Type != nil {
		partnerChannelCheck.Type = *partnerChannel.Type
	}
	if partnerChannel.LogoUrl != nil {
		partnerChannelCheck.LogoUrl = *partnerChannel.LogoUrl
	}
	if partnerChannel.IsActive != nil {
		partnerChannelCheck.IsActive = *partnerChannel.IsActive
	}
	if partnerChannel.NotifyUser != nil {
		partnerChannelCheck.NotifyUser = *partnerChannel.NotifyUser
	}

	// we mapping and save the data
	err = s.repo.Update(ctx, partnerChannelCheck)
	if err != nil {
		return errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "UpdatePartnerChannel Update repo")
	}
	return nil
}

func (s *service) FindPartnerChannelById(ctx context.Context, partnerID int) (*response.PartnerChannel, error) {
	res, err := s.repo.FindByPartnerID(ctx, partnerID)
	if err != nil {
		return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "FindPartnerChannelById FindByPartnerID repo")
	}
	if res == nil {
		return nil, errors.ErrKindNotFound(errors.ErrCodePartnerChannelNotFound, "FindPartnerChannelById check")
	}
	resp := s.mapDomainToResponse(res)
	return &resp, nil
}

// TODO move to request
func (s *service) mapDomainToResponse(ResPartnerChannel *domain.PartnerChannel) response.PartnerChannel {
	return response.PartnerChannel{
		ID:                     ResPartnerChannel.ID,
		PartnerID:              ResPartnerChannel.PartnerID,
		Name:                   ResPartnerChannel.Name,
		DisplayName:            ResPartnerChannel.DisplayName,
		Code:                   ResPartnerChannel.Code,
		SecretKey:              ResPartnerChannel.SecretKey,
		GrpCode:                ResPartnerChannel.GrpCode,
		Flag:                   ResPartnerChannel.Flag,
		Type:                   ResPartnerChannel.Type,
		LogoUrl:                ResPartnerChannel.LogoUrl,
		MinimumAmount:          ResPartnerChannel.MinimumAmount,
		IsManualPaymentAllowed: ResPartnerChannel.IsManualPaymentAllowed,
		Ordering:               ResPartnerChannel.Ordering,
		IsActive:               ResPartnerChannel.IsActive,
		NotifyUser:             ResPartnerChannel.NotifyUser,
		DescriptionCode:        ResPartnerChannel.DescriptionCode,
	}
}

// TODO move to request
func (s *service) mapRequestToDomain(ReqPartnerChannel *request.PartnerChannel) domain.PartnerChannel {
	return domain.PartnerChannel{
		PartnerID:              ReqPartnerChannel.PartnerID,
		Name:                   ReqPartnerChannel.Name,
		DisplayName:            ReqPartnerChannel.DisplayName,
		Code:                   ReqPartnerChannel.Code,
		SecretKey:              ReqPartnerChannel.SecretKey,
		GrpCode:                ReqPartnerChannel.GrpCode,
		Flag:                   ReqPartnerChannel.Flag,
		Type:                   ReqPartnerChannel.Type,
		LogoUrl:                ReqPartnerChannel.LogoUrl,
		MinimumAmount:          ReqPartnerChannel.MinimumAmount,
		IsManualPaymentAllowed: ReqPartnerChannel.IsManualPaymentAllowed,
		Ordering:               ReqPartnerChannel.Ordering,
		IsActive:               ReqPartnerChannel.IsActive,
		NotifyUser:             ReqPartnerChannel.NotifyUser,
		DescriptionCode:        ReqPartnerChannel.DescriptionCode,
		CreatedBy:              "1",
		UpdatedBy:              "1",
	}
}

func NewService(repo domain.Repository, repoPartner domainPartner.Repository) Service {
	return &service{
		repo:        repo,
		repoPartner: repoPartner,
	}
}
