package partnerChannel

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	ferror2 "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
	MockPartnerRepo "bitbucket.org/finaccelteam/onboarding/domain/partner/mocks"
	domain "bitbucket.org/finaccelteam/onboarding/domain/partnerChannel"
	MockPartnerChannelRepo "bitbucket.org/finaccelteam/onboarding/domain/partnerChannel/mocks"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"bitbucket.org/finaccelteam/onboarding/request"
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"testing"
)

//var mockCtx = mock.AnythingOfTypeArgument("*context.emptyCtx")

type partnerChannelTestCase struct {
	name              string
	partnerID         int
	request           *request.PartnerChannel
	requestUpdate     *request.UpdatePartnerChannel
	wantErr           bool
	expectedErrorKind ferror2.Kind
	expectedErrorCode ferror.Code
	justPrepareMock   func(t *testing.T)
	justAssertCalled  func(t *testing.T)
}

// set the dependencies need for interfaces
type partnerChannelTestSuite struct {
	suite.Suite
	repoPartnerChannel MockPartnerChannelRepo.Repository
	repoPartner        MockPartnerRepo.Repository
	svcPartnerChannel  Service
	ctx                context.Context
}

// preparing mock using define anything what interface needs for mock testing here
func (s *partnerChannelTestSuite) SetupTest() {
	s.repoPartnerChannel = MockPartnerChannelRepo.Repository{}
	s.repoPartner = MockPartnerRepo.Repository{}
	s.svcPartnerChannel = NewService(&s.repoPartnerChannel, &s.repoPartner)
	s.ctx = context.Background()
}

func (s *partnerChannelTestSuite) TestCreatePartnerChannel() {
	testCases := []partnerChannelTestCase{
		{
			name:              "partner channel update, but request is missing",
			wantErr:           true,
			expectedErrorKind: errors.ErrUnprocessableKind,
			expectedErrorCode: errors.ErrCodeBadRequest,
		},
		{
			name:    "partner channel repo FindByID, partner channel PartnerID found and create partner channel",
			request: s.reqPartnerChannel(10, "Bank Mandiri Jaya", "MandiriJaya", 10, true, true),
			wantErr: false,
			justPrepareMock: func(t *testing.T) {
				partnerChannel := &domain.PartnerChannel{ID: 1, PartnerID: 10}
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(nil, nil).Once()
				s.repoPartnerChannel.On("Save", mock.Anything, mock.Anything).Return(partnerChannel.ID, nil).Run(s.validatePartnerChannel(t, s.mockPartnerChannel()))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
				s.repoPartnerChannel.On("Save", mock.Anything)
			},
		},
		{
			name:              "partner channel repo FindByID, partner channel PartnerID found but create partner channel failed",
			request:           s.reqPartnerChannel(10, "Bank Mandiri Jaya", "MandiriJaya", 10, true, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(nil, nil).Once()
				s.repoPartnerChannel.On("Save", mock.Anything, mock.Anything).Return(-1, ferror.E("some error")).Run(s.validatePartnerChannel(t, s.mockPartnerChannel()))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
				s.repoPartnerChannel.On("Save", mock.Anything, mock.Anything)
			},
		},
		{
			name:              "partner channel repo FindByID, partner channel PartnerID is not exists in partner",
			request:           s.reqPartnerChannel(20, "Bank Mandiri", "Mandiri", 1, true, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(nil, ferror.E("some error")).Once()
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
			},
		},
		{
			name:              "partner channel repo FindByID, partner channel Creating error",
			request:           s.reqPartnerChannel(20, "Bank Mandiri", "Mandiri", 1, true, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(nil, ferror.E("some error")).Once()
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
			},
		},
	}

	for _, testCase := range testCases {
		s.T().Run(testCase.name, func(t *testing.T) {
			s.SetupTest()

			if testCase.request != nil {
				testCase.justPrepareMock(t)
			}

			_, err := s.svcPartnerChannel.CreatePartnerChannel(s.ctx, testCase.request)

			if testCase.wantErr {
				assert.True(t, ferror.Is(err, testCase.expectedErrorCode), "error expected: %v, but got: %v", testCase.expectedErrorCode, err)
				assert.True(t, ferror2.IsOfKind(err, testCase.expectedErrorKind), "error expected: %v, but got: %v", testCase.expectedErrorKind, err)
			}

			if testCase.request != nil {
				testCase.justAssertCalled(t)
			}
		})
	}
}

func (s *partnerChannelTestSuite) TestFindPartnerChannelById() {
	// set all test cases here
	testCases := []partnerChannelTestCase{
		{
			name:    "partner channel repo FindPartnerChannelById, partner channel exists",
			request: s.reqPartnerChannel(21, "Bank Mandiri", "Mandiri", 1, true, true),
			wantErr: false,
			justPrepareMock: func(t *testing.T) {
				partnerChannel := domain.PartnerChannel{ID: 10, PartnerID: 21, Name: "Bank Mandiri", DisplayName: "Mandiri", IsActive: true}
				s.repoPartnerChannel.On("FindByPartnerID", mock.Anything, mock.Anything).Return(&partnerChannel, nil)
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartnerChannel.AssertCalled(t, "FindByPartnerID", mock.Anything, mock.Anything)
			},
		},
		{
			name:              "partner channel repo FindPartnerChannelById, partner channel error",
			request:           s.reqPartnerChannel(21, "Bank Mandiri", "Mandiri", 1, true, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			justPrepareMock: func(t *testing.T) {
				s.repoPartnerChannel.On("FindByPartnerID", mock.Anything, mock.Anything).Return(nil, ferror.E("some errors")).Once()
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartnerChannel.AssertCalled(t, "FindByPartnerID", mock.Anything, mock.Anything)
			},
		},
		{
			name:              "partner channel repo FindPartnerChannelById, partner channel not found",
			request:           s.reqPartnerChannel(19, "Bank Mandiri", "Mandiri", 1, true, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrNotFoundKind,
			expectedErrorCode: errors.ErrCodePartnerChannelNotFound,
			justPrepareMock: func(t *testing.T) {
				s.repoPartnerChannel.On("FindByPartnerID", mock.Anything, mock.Anything).Return(nil, nil)
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartnerChannel.AssertCalled(t, "FindByPartnerID", mock.Anything, mock.Anything)
			},
		},
	}

	// we loop all test cases
	for _, testCase := range testCases {
		// run every test cases
		s.T().Run(testCase.name, func(t *testing.T) {
			// load the initialize setup
			s.SetupTest()

			//  if request is not null, we set the prepareMock value
			if testCase.request != nil {
				testCase.justPrepareMock(t)
			}

			// call service
			_, err := s.svcPartnerChannel.FindPartnerChannelById(s.ctx, testCase.request.PartnerID)

			// if wantErr true, so we check are the error code is same as expected
			if testCase.wantErr {
				assert.True(t, ferror.Is(err, testCase.expectedErrorCode), "error expected: %v, but got: %v", testCase.expectedErrorCode, err)
				assert.True(t, ferror2.IsOfKind(err, testCase.expectedErrorKind), "error expected: %v, but got: %v", testCase.expectedErrorKind, err)
			}

			// assert if wantErr true, err is not null
			assert.Equal(t, testCase.wantErr, err != nil, "error expected %v, but got: %v", testCase.wantErr, err)

			// if request is not null, we set the assertCalled
			// it means, make sure the methods is called
			if testCase.request != nil {
				testCase.justAssertCalled(t)
			}

		})
	}

}

func (s *partnerChannelTestSuite) TestUpdatePartnerChannel() {
	testCases := []partnerChannelTestCase{
		{
			name:              "partner channel update, but request is missing",
			wantErr:           true,
			expectedErrorKind: errors.ErrUnprocessableKind,
			expectedErrorCode: errors.ErrCodeBadRequest,
		},
		{
			name:              "partner channel repo FindByID, partner channel error",
			partnerID:         10,
			requestUpdate:     s.reqUpdatePartnerChannel("Bank Mandiri Jaya", "MandiriJaya", 10, true, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			justPrepareMock: func(t *testing.T) {
				s.repoPartnerChannel.On("FindByID", mock.Anything, mock.Anything).Return(nil, ferror.E("some error"))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartnerChannel.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
			},
		},
		{
			name:              "partner channel repo FindByID, partner channel is not exist",
			partnerID:         10,
			requestUpdate:     s.reqUpdatePartnerChannel("Bank Mandiri Jaya", "MandiriJaya", 10, true, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodePartnerChannelNotFound,
			justPrepareMock: func(t *testing.T) {
				s.repoPartnerChannel.On("FindByID", mock.Anything, mock.Anything).Return(nil, nil)
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartnerChannel.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
			},
		},
		{
			name:          "partner channel repo FindByID, partner channel update",
			partnerID:     10,
			requestUpdate: s.reqUpdatePartnerChannel("Bank Mandiri Jaya", "MandiriJaya", 10, true, true),
			wantErr:       false,
			justPrepareMock: func(t *testing.T) {
				partnerChannel := &domain.PartnerChannel{
					PartnerID:   10,
					Name:        "Testing",
					DisplayName: "Testing",
					Type:        1,
					IsActive:    false,
					NotifyUser:  true,
				}
				s.repoPartnerChannel.On("FindByID", mock.Anything, mock.Anything).Return(partnerChannel, nil)
				s.repoPartnerChannel.On("Update", mock.Anything, mock.Anything).Return(nil).Run(s.validatePartnerChannel(t, s.mockUpdatePartnerChannel()))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartnerChannel.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
				s.repoPartnerChannel.AssertCalled(t, "Update", mock.Anything, mock.Anything)
			},
		},
		{
			name:              "partner channel repo update, error",
			partnerID:         10,
			requestUpdate:     s.reqUpdatePartnerChannel("Bank Mandiri Jaya", "MandiriJaya", 10, true, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			justPrepareMock: func(t *testing.T) {
				partnerChannel := &domain.PartnerChannel{
					PartnerID:   10,
					Name:        "Testing",
					DisplayName: "Testing",
					Type:        1,
					IsActive:    false,
					NotifyUser:  true,
				}
				s.repoPartnerChannel.On("FindByID", mock.Anything, mock.Anything).Return(partnerChannel, nil)
				s.repoPartnerChannel.On("Update", mock.Anything, mock.Anything).Return(ferror.E("some error")).Run(s.validatePartnerChannel(t, s.mockUpdatePartnerChannel()))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartnerChannel.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
				s.repoPartnerChannel.AssertCalled(t, "Update", mock.Anything, mock.Anything)
			},
		},
	}

	for _, testCase := range testCases {
		s.T().Run(testCase.name, func(t *testing.T) {
			s.SetupTest()

			if testCase.requestUpdate != nil {
				testCase.justPrepareMock(t)
			}

			err := s.svcPartnerChannel.UpdatePartnerChannel(s.ctx, testCase.requestUpdate, testCase.partnerID)

			if testCase.wantErr {
				assert.True(t, ferror.Is(err, testCase.expectedErrorCode), "error expected: %v, but got: %v", testCase.expectedErrorCode, err)
				assert.True(t, ferror2.IsOfKind(err, testCase.expectedErrorKind), "error expected: %v, but got: %v", testCase.expectedErrorKind, err)
			}

			if testCase.requestUpdate != nil {
				testCase.justAssertCalled(t)
			}
		})
	}
}

func (s *partnerChannelTestSuite) validatePartnerChannel(t *testing.T, expected *domain.PartnerChannel) func(args mock.Arguments) {
	return func(args mock.Arguments) {
		actualPartnerChannel := args[1].(*domain.PartnerChannel)
		assert.Equal(t, expected.Name, actualPartnerChannel.Name, "expected %v found %v", expected.Name, actualPartnerChannel.Name)
		assert.Equal(t, expected.DisplayName, actualPartnerChannel.DisplayName, "expected %v found %v", expected.DisplayName, actualPartnerChannel.DisplayName)
		assert.Equal(t, expected.PartnerID, actualPartnerChannel.PartnerID, "expected %v found %v", expected.PartnerID, actualPartnerChannel.PartnerID)
		assert.Equal(t, expected.Type, actualPartnerChannel.Type, "expected %v found %v", expected.Type, actualPartnerChannel.Type)
		assert.Equal(t, expected.IsActive, actualPartnerChannel.IsActive, "expected %v found %v", expected.IsActive, actualPartnerChannel.IsActive)
		assert.Equal(t, expected.NotifyUser, actualPartnerChannel.NotifyUser, "expected %v found %v", expected.NotifyUser, actualPartnerChannel.NotifyUser)
		expected.ID = actualPartnerChannel.ID
		expected.Name = actualPartnerChannel.Name
		expected.DisplayName = actualPartnerChannel.DisplayName
		expected.PartnerID = actualPartnerChannel.PartnerID
		expected.Type = actualPartnerChannel.Type
		expected.IsActive = actualPartnerChannel.IsActive
		expected.NotifyUser = actualPartnerChannel.NotifyUser
		expected.CreatedBy = actualPartnerChannel.CreatedBy
		expected.UpdatedBy = actualPartnerChannel.UpdatedBy
		assert.EqualValues(t, expected, actualPartnerChannel)
	}
}

func (s *partnerChannelTestSuite) reqPartnerChannel(partnerID int, name string, displayName string, typePartner int, notifyUser bool, isActive bool) *request.PartnerChannel {
	return &request.PartnerChannel{
		PartnerID:   partnerID,
		Name:        name,
		DisplayName: displayName,
		Type:        typePartner,
		NotifyUser:  notifyUser,
		IsActive:    isActive,
	}
}

func (s partnerChannelTestSuite) reqUpdatePartnerChannel(name string, displayName string, typePartner int, isActive bool, notifyUser bool) *request.UpdatePartnerChannel {
	return &request.UpdatePartnerChannel{
		Name:        &name,
		DisplayName: &displayName,
		Type:        &typePartner,
		LogoUrl:     nil,
		IsActive:    &isActive,
		NotifyUser:  &notifyUser,
	}
}

func (s partnerChannelTestSuite) mockPartnerChannel() *domain.PartnerChannel {
	return &domain.PartnerChannel{
		PartnerID:   10,
		Name:        "Bank Mandiri Jaya",
		DisplayName: "MandiriJaya",
		Type:        10,
		IsActive:    true,
		NotifyUser:  true,
	}
}

func (s partnerChannelTestSuite) mockUpdatePartnerChannel() *domain.PartnerChannel {
	return &domain.PartnerChannel{
		PartnerID:   10,
		Name:        "Bank Mandiri Jaya",
		DisplayName: "MandiriJaya",
		Type:        10,
		IsActive:    true,
		NotifyUser:  true,
	}
}

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func TestPartnerTestSuite(t *testing.T) {
	suite.Run(t, new(partnerChannelTestSuite))
}
