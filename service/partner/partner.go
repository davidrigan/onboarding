package partner

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/onboarding/domain/partner"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
	"context"
	"encoding/json"
	"fmt"
)

type Service interface {
	GetPartners(ctx context.Context) ([]response.Partner, error)
	GetPartner(ctx context.Context, id int) (*response.Partner, error)
	CreatePartner(ctx context.Context, partner *request.Partner) (int, error)
	UpdatePartner(ctx context.Context, partner *request.UpdatePartner, id int) error
	GetPartnerByMerchantID(ctx context.Context, merchantID string) ([]response.Partner, error)
	FilterPartner(ctx context.Context, merchantID string, gatewayType int) ([]response.Partner, error)
	PublishCreatePartner(ctx context.Context, eventID string, partner *request.Partner, createPartnerPublisher broker.Publisher) error
	ConsumeCreatePartner(ctx context.Context, partner *request.Partner) error
}
type service struct {
	repo partner.Repository
}

func (s *service) GetPartners(ctx context.Context) ([]response.Partner, error) {
	partners, err := s.repo.Get(ctx)
	if err != nil {
		return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "GetPartners Get Repo")
	}
	res := make([]response.Partner, 0)
	for _, p := range partners {
		tPartner := s.mapDomainToResponse(p)
		res = append(res, tPartner)
	}
	return res, nil
}

func (s *service) GetPartner(ctx context.Context, id int) (*response.Partner, error) {
	partnerResp, err := s.repo.FindByID(ctx, id)
	if err != nil {
		return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "GetPartner FindByID Repo")
	}
	if partnerResp == nil {
		return nil, errors.ErrKindNotFound(errors.ErrCodePartnerNotFound, "GetPartner check")
	}
	resp := s.mapDomainToResponse(partnerResp)
	return &resp, nil
}

func (s *service) CreatePartner(ctx context.Context, partner *request.Partner) (int, error) {
	if partner == nil {
		return -1, errors.ErrKindUnprocessable(errors.ErrCodeBadRequest, "CreatePartner partner request")
	}
	req := s.mapRequestToDomain(partner)
	id, err := s.repo.Save(ctx, &req)
	if err != nil {
		return 0, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "CreatePartner Save Repo")
	}
	return id, nil
}

func (s *service) UpdatePartner(ctx context.Context, request *request.UpdatePartner, id int) error {
	if request == nil {
		return errors.ErrKindUnprocessable(errors.ErrCodeBadRequest, "CreatePartner partner request")
	}
	// we check first if data is exist
	partnerCheck, err := s.repo.FindByID(ctx, id)
	if err != nil {
		return errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "UpdatePartner FindByID Repo")
	}
	// if data not exists
	if partnerCheck == nil {
		return errors.ErrKindNotFoundWithCause(errors.ErrCodePartnerNotFound, err, "UpdatePartner check")
	}

	// check if field is not nil
	if request.MerchantID != nil {
		partnerCheck.MerchantID = *request.MerchantID
	}
	if request.Name != nil {
		partnerCheck.Name = *request.Name
	}
	if request.GatewayType != nil {
		partnerCheck.GatewayType = *request.GatewayType
	}
	if request.IsActive != nil {
		partnerCheck.IsActive = *request.IsActive
	}

	// we mapping and save the data
	err = s.repo.Update(ctx, partnerCheck)
	if err != nil {
		return errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "UpdatePartner Update Repo")
	}

	return nil
}

func (s *service) FilterPartner(ctx context.Context, merchantID string, gatewayType int) ([]response.Partner, error) {
	var partnerResp []*partner.Partner
	if gatewayType != 0 && merchantID != "" {
		merchantIDAndGatewayTypePartner, err := s.repo.FindByMerchantIDAndGatewayType(ctx, merchantID, gatewayType)
		if err != nil {
			return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "Partner FindByMerchantIDAndGatewayType Repo")
		}
		if len(merchantIDAndGatewayTypePartner) == 0 {
			return nil, errors.ErrKindNotFoundWithCause(errors.ErrCodePartnerNotFound, err, "Partner Not Found for FindByMerchantIDAndGatewayType")
		}
		partnerResp = merchantIDAndGatewayTypePartner
	} else if gatewayType != 0 {
		gatewayTypePartner, err := s.repo.FindByGatewayType(ctx, gatewayType)
		if err != nil {
			return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "Partner FindByGatewayType Repo")
		}
		if len(gatewayTypePartner) == 0 {
			return nil, errors.ErrKindNotFoundWithCause(errors.ErrCodePartnerNotFound, err, "Partner Not Found for FindByGatewayType")
		}
		partnerResp = gatewayTypePartner
	} else if merchantID != "" {
		merchantIDPartner, err := s.repo.FindByMerchantID(ctx, merchantID)
		if err != nil {
			return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "Partner FindByMerchantID Repo")
		}
		if len(merchantIDPartner) == 0 {
			return nil, errors.ErrKindNotFoundWithCause(errors.ErrCodePartnerNotFound, err, "Partner Not Found for merchantIDPartner")
		}
		partnerResp = merchantIDPartner
	} else {
		allPartners, err := s.repo.Get(ctx)
		if err != nil {
			return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "Partner Get Repo")
		}
		if len(allPartners) == 0 {
			return nil, errors.ErrKindNotFoundWithCause(errors.ErrCodePartnerNotFound, err, "Partner Not Found for Get")
		}
		partnerResp = allPartners
	}
	res := make([]response.Partner, 0)
	for _, p := range partnerResp {
		pmap := s.mapDomainToResponse(p)
		res = append(res, pmap)
	}
	return res, nil
}

func (s *service) GetPartnerByMerchantID(ctx context.Context, merchantID string) ([]response.Partner, error) {
	partnerResp, err := s.repo.FindByMerchantID(ctx, merchantID)
	if err != nil {
		return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "GetPartnerByMerchantID FindByMerchantID Repo")
	}
	res := make([]response.Partner, 0)
	for _, p := range partnerResp {
		pmap := s.mapDomainToResponse(p)
		res = append(res, pmap)
	}
	return res, nil
}

func (s *service) PublishCreatePartner(ctx context.Context, eventID string, partner *request.Partner, createPartnerPublisher broker.Publisher) error {
	// marshall data
	data, err := json.Marshal(&partner)
	if err != nil {
		return errors.ErrKindUnprocessableWithCause(errors.ErrCodeInvalidDecode, err, "Marshall request")
	}
	// topic name
	topicName := "test"
	// eventID name
	eventID = fmt.Sprintf("create-partner-%v", eventID)
	if err = createPartnerPublisher.Publish(ctx, topicName, eventID, data); err != nil {
		return broker.NewError(broker.ErrPublisher, err, "publish create partner")
	}
	logger.Info(ctx, "Published event with eventID %s done", eventID)
	return nil
}

func (s *service) ConsumeCreatePartner(ctx context.Context, partner *request.Partner) error {
	req := s.mapRequestToDomain(partner)
	id, err := s.repo.Save(ctx, &req)
	if err != nil {
		return errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "Partner Save Repo")
	}
	if id != 0 {
		logger.Info(ctx, "success save data from publisher")
	}
	return nil
}

// TODO move to response
func (s *service) mapDomainToResponse(partner *partner.Partner) response.Partner {
	return response.Partner{
		ID:          partner.ID,
		MerchantID:  partner.MerchantID,
		Name:        partner.Name,
		IsActive:    partner.IsActive,
		GatewayType: partner.GatewayType,
		EntityID:    partner.EntityID,
	}
}

// TODO move to request
func (s *service) mapRequestToDomain(request *request.Partner) partner.Partner {
	return partner.Partner{
		MerchantID:  request.MerchantID,
		Name:        request.Name,
		GatewayType: request.GatewayType,
		EntityID:    request.EntityID,
		IsActive:    *request.IsActive,
		CreatedBy:   "1",
		UpdatedBy:   "1",
	}
}

func NewService(repo partner.Repository) Service {
	return &service{
		repo: repo,
	}
}
