-- +goose Up
-- +goose StatementBegin
SELECT 'up SQL query';
-- +goose StatementEnd

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema payment
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `payment` DEFAULT CHARACTER SET utf8;
SHOW WARNINGS;
USE `payment`;

-- -----------------------------------------------------
-- Table `payment`.`partner`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`partner` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(128)   NOT NULL,
    `merchant_id` VARCHAR(128)   NOT NULL,
    `gateway_type` SMALLINT(5)  NOT NULL,
    `is_active` TINYINT   NOT NULL,
    `entity_id` INT   NOT NULL,
    `timezone` VARCHAR(32)   NOT NULL,
    `version` INT   NOT NULL,
    `created_at` TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT   NOT NULL,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` INT   NOT NULL,
    `deleted_at` TIMESTAMP   NULL,
    `deleted_by` INT   NULL,
    PRIMARY KEY (`id`)
    )
ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`partner_channel`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`partner_channel` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `partner_id` INT   NOT NULL,
    `name` VARCHAR(128)   NOT NULL,
    `display_name` VARCHAR(128)   NOT NULL,
    `code` VARCHAR(64)   NOT NULL,
    `secret_key` VARCHAR(4096)   NOT NULL,
    `flag` VARCHAR(64)   NOT NULL,
    `grpCode` SMALLINT(5)  NOT NULL,
    `type` SMALLINT(5)  NOT NULL,
    `logo_url` VARCHAR(256)   NULL,
    `minimum_amount` INT   NOT NULL,
    `is_manual_payment_allowed` TINYINT(1)   NOT NULL,
    `ordering` SMALLINT(5)   NOT NULL,
    `is_active` TINYINT   NOT NULL,
    `notify_user` TINYINT   NOT NULL,
    `description_code` VARCHAR(128)   NOT NULL,
    `timezone` VARCHAR(32)   NOT NULL,
    `version` INT  NOT NULL,
    `created_at` TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT   NOT NULL,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` INT   NOT NULL,
    `deleted_at` TIMESTAMP   NULL,
    `deleted_by` INT   NULL,
    CONSTRAINT `pk_PartnerChannel` PRIMARY KEY (`id`)
    )
ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`virtual_account`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`virtual_account` (
   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
   `user_id` INT   NOT NULL,
   `virtual_account_id`  VARCHAR(256)  NOT NULL,
   `channel_id` INT   NULL,
   `gateway_id` INT   NOT NULL,
   `is_active` TINYINT   NOT NULL,
    `entity_ID` INT  NOT NULL,
    `timezone` VARCHAR(32)   NOT NULL,
    `version` INT  NOT NULL,
    `created_at` TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT   NOT NULL,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` INT   NOT NULL,
    `deleted_at` TIMESTAMP   NULL,
    `deleted_by` INT   NULL,
    CONSTRAINT `pk_VirtualAccount` PRIMARY KEY (`id`)
    )
ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`user_repayment`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`user_repayment` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT   NOT NULL,
    `partner_payment_id` INT   NOT NULL,
    `amount` DECIMAL(25,2)   NOT NULL,
    `payment_time` INT   NOT NULL,
    `allocation_time` INT   NOT NULL,
    `status` TINYINT   NOT NULL,
    `unprocessed_amount` DECIMAL(25,2)   NOT NULL,
    `timezone` VARCHAR(32)   NOT NULL,
    `repayment_config_id` INT   NOT NULL,
    `version` INT   NOT NULL,
    `created_at` TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT   NOT NULL,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` INT   NOT NULL,
    `deleted_at` TIMESTAMP   NULL,
    `deleted_by` INT   NULL,
    CONSTRAINT `pk_user_repayment` PRIMARY KEY (`id`)
    )
ENGINE = InnoDB;
SHOW WARNINGS;


-- -----------------------------------------------------
-- Table `payment`.`payment_allocation`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`payment_allocation` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `payment_id` INT   NOT NULL,
    `allocation_type` SMALLINT(5)  NOT NULL,
    `amount_paid` DECIMAL(25,2)   NOT NULL,
    `meta_info` Json   NOT NULL,
    `timezone` VARCHAR(32)   NOT NULL,
    `created_at` INT   NOT NULL,
    CONSTRAINT `pk_payment_allocation` PRIMARY KEY (`id`)
    )
ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`partner_payment`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`partner_payment` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT   NOT NULL,
    `channel_id` INT   NOT NULL,
    `reference_id` INT   NOT NULL,
    `expected_amount` DECIMAL(25,2)   NOT NULL,
    `confirmed_amount` DECIMAL(25,2)   NOT NULL,
    `payment_time` INT   NOT NULL,
    `payment_status` SMALLINT(5)  NOT NULL,
    `entity_id` INT   NOT NULL,
    `timezone` VARCHAR(32)   NOT NULL,
    -- signature key will be part of meta info
    `meta_info` Json   NOT NULL,
    `version` INT  NOT NULL,
    `created_at` TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT   NOT NULL,
    `updated_at` TIMESTAMP   NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` INT   NOT NULL,
    `deleted_at` TIMESTAMP   NULL,
    `deleted_by` INT   NULL,
    CONSTRAINT `pk_partner_payment` PRIMARY KEY (`id`)
    )
ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`entity`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`entity` (
   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
   `entity_key` VARCHAR(64)   NOT NULL,
   `country` VARCHAR(32)   NOT NULL,
   `timezone` VARCHAR(32)   NOT NULL,
   `version` INT   NOT NULL,
    `created_at` TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT   NOT NULL,
    `updated_at` TIMESTAMP   NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` INT   NOT NULL,
    `deleted_at` TIMESTAMP   NULL,
    `deleted_by` INT   NULL,
    CONSTRAINT `pk_entity` PRIMARY KEY (`id`)
   )

ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`repayment_config`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`repayment_config` (
   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
   `entity_id` INT   NOT NULL,
   `config` Json   NOT NULL,
   `timezone` VARCHAR(32)   NOT NULL,
   `status` TINYINT  NOT NULL,
   `version` INT   NOT NULL,
    `created_at` TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT   NOT NULL,
    `updated_at` TIMESTAMP   NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` INT   NOT NULL,
    `deleted_at` TIMESTAMP   NULL,
    `deleted_by` INT   NULL,
    CONSTRAINT `pk_repayment_config` PRIMARY KEY (`id`)
   )
ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`user_detail`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`user_detail` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(128) NOT NULL,
    `last_name` VARCHAR(128) NOT NULL,
    `kredivo_user_id` int NOT NULL,
    `external_reference_id` VARCHAR(64) NOT NULL,
    `mobile_number` VARCHAR(32) NOT NULL,
    `email` VARCHAR(128) NOT NULL,
    `is_active` TINYINT NOT NULL,
    `entity_id` INT NOT NULL,
    `timezone` VARCHAR(32) NOT NULL,
    `version` INT NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT NOT NULL,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` INT NOT NULL,
    `deleted_at` TIMESTAMP NULL,
    `deleted_by` INT NULL,
    CONSTRAINT `pk_user_detail` PRIMARY KEY (`id`)
    )
    ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`feature_config`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`feature_config` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(64) NOT NULL,
    `feature_type` SMALLINT(5) UNSIGNED NOT NULL,
    `description` VARCHAR(256) NOT NULL,
    `is_active` TINYINT NOT NULL,
    `version` INT UNSIGNED NOT NULL,
    `created_at` TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT   NOT NULL,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` INT   NOT NULL,
    `deleted_at` TIMESTAMP   NULL,
    `deleted_by` INT   NULL,
    PRIMARY KEY (`id`))
ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`idempotency`
-- Implement Save()
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`idempotency` (
     `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `idempotency_key` VARCHAR(256) NOT NULL,
    `request_path` VARCHAR(256) NOT NULL,
    `request_param` VARCHAR(4096) NOT NULL,
    `request_method` VARCHAR(16) NOT NULL,
    `locked_at` TIMESTAMP NULL,
    `response_code` SMALLINT(5) UNSIGNED NULL,
    `response_body` VARCHAR(4096) NULL,
    `version` INT UNSIGNED NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` TIMESTAMP NULL,
    PRIMARY KEY (`id`)
)
ENGINE = InnoDB;
SHOW WARNINGS;
-- -----------------------------------------------------
-- Table `payment`.`incoming_event_type`
-- Type: enum.event
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`incoming_event_type` (
    `incoming_event_type_id` SMALLINT(5) UNSIGNED NOT NULL,
    `name` VARCHAR(32) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` TIMESTAMP NULL,
    PRIMARY KEY (`incoming_event_type_id`),
    UNIQUE INDEX `incoming_event_type_name` (`name`))
ENGINE = InnoDB;
SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `payment`.`incoming_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment`.`incoming_event` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `event_id` VARCHAR(256) NOT NULL,
    `event_type` SMALLINT(5) UNSIGNED NOT NULL,
    `version` INT UNSIGNED NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NULL NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` TIMESTAMP NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `event_id_event_type` (`event_id` ASC, `event_type` ASC))
ENGINE = InnoDB;
SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
-- +goose StatementEnd
