package errors

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	ferror2 "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
)

const (
	// ErrRepositoryCode is the super-type for any error that is coming from the repository layer.
	//
	// This error type IS eligible for retry.
	ErrRepositoryCode = ferror.Code("ONBOARDING-1001")

	// ErrBusinessValidationCode is anything that fails our business requirement.
	//
	// This error type IS eligible for retry.
	ErrBusinessValidationCode = ferror.Code("ONBOARDING-1002")

	// ErrUnprocessableCode is for any condition that will fail even on multiple retries, so we need Developer intervention
	// to solve these types of errors.
	//
	// This error type is NOT eligible for retry.
	ErrUnprocessableCode = ferror.Code("ONBOARDING-1003")

	// ErrInternalCode is any other error that can occur during execution.
	//
	// This error type IS eligible for retry.
	ErrInternalCode = ferror.Code("ONBOARDING-1004")

	ErrNotFoundCode = ferror.Code("ONBOARDING-1005")

	IdempotencyKeyMissing = ferror.Code("ONBOARDING-1006")

	// Const of V2 beneath
	//
	// .....

	// ErrBusinessValidationKind is anything that fails our business requirement.
	ErrBusinessValidationKind = ferror2.Kind("BUSINESS_VALIDATION")

	// ErrUnprocessableKind is for process when we retries many times, the results will be same error
	// and developer need fix it
	ErrUnprocessableKind = ferror2.Kind("UNPROCESSABLE")

	// ErrInternalServerErrorKind is for error in third party, like db down
	// if user retires, and have change server give results
	ErrInternalServerErrorKind = ferror2.Kind("INTERNAL_SERVER_ERROR")

	// ErrNotFoundKind is when result of request is empty or we cannot find
	ErrNotFoundKind = ferror2.Kind("NOT_FOUND")
)

func ErrRepository(m string, args ...interface{}) error {
	return ferror.Newf(ErrRepositoryCode, m, args...)
}

func ErrRepositoryWithCause(err error, m string, args ...interface{}) error {
	return ferror.NewWithCausef(ErrRepositoryCode, err, m, args...)
}

func ErrBusinessValidation(m string, args ...interface{}) error {
	return ferror.Newf(ErrBusinessValidationCode, m, args...)
}

func ErrBusinessValidationWithCause(err error, m string, args ...interface{}) error {
	return ferror.NewWithCausef(ErrBusinessValidationCode, err, m, args...)
}

func ErrUnprocessable(m string, args ...interface{}) error {
	return ferror.Newf(ErrUnprocessableCode, m, args...)
}

func ErrUnprocessableWithCause(err error, m string, args ...interface{}) error {
	return ferror.NewWithCausef(ErrUnprocessableCode, err, m, args...)
}

func ErrInternal(m string, args ...interface{}) error {
	return ferror.Newf(ErrInternalCode, m, args...)
}

func ErrInternalWithCause(err error, m string, args ...interface{}) error {
	return ferror.NewWithCausef(ErrInternalCode, err, m, args...)
}

func ErrNotFound(m string, args ...interface{}) error {
	return ferror.Newf(ErrNotFoundCode, m, args...)
}

func ErrNotFoundWithCause(err error, m string, args ...interface{}) error {
	return ferror.NewWithCausef(ErrNotFoundCode, err, m, args...)
}

// New V2 Error Wrapper beneath

func ErrKindBusinessValidationWithCause(code ferror.Code, cause error, m string) error {
	return ferror2.NewWithCause(ErrBusinessValidationKind, code, cause, m)
}

func ErrKindUnprocessable(code ferror.Code, m string) error {
	return ferror2.New(ErrUnprocessableKind, code, m)
}

func ErrKindUnprocessableWithCause(code ferror.Code, cause error, m string) error {
	return ferror2.NewWithCause(ErrUnprocessableKind, code, cause, m)
}

func ErrKindInternalServerError(code ferror.Code, m string) error {
	return ferror2.New(ErrInternalServerErrorKind, code, m)
}

func ErrKindInternalServerErrorWithCause(code ferror.Code, cause error, m string) error {
	return ferror2.NewWithCause(ErrInternalServerErrorKind, code, cause, m)
}

func ErrKindNotFound(code ferror.Code, m string) error {
	return ferror2.New(ErrNotFoundKind, code, m)
}

func ErrKindNotFoundWithCause(code ferror.Code, cause error, m string) error {
	return ferror2.NewWithCause(ErrNotFoundKind, code, cause, m)
}
