package request

type Partner struct {
	Name        string `validate:"required,min=1" json:"name"`
	MerchantID  string `validate:"required,min=1" json:"merchant_id"`
	GatewayType int    `validate:"required,min=1" json:"gateway_type"`
	IsActive    *bool  `validate:"required" json:"is_active"`
	EntityID    int    `validate:"required,min=1" json:"entity_id"`
}

type UpdatePartner struct {
	Name        *string `json:"name"`
	MerchantID  *string `json:"merchant_id"`
	GatewayType *int    `json:"gateway_type"`
	IsActive    *bool   `json:"is_active"`
}
