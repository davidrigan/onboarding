package request

import "bitbucket.org/finaccelteam/ms-common-go/sql"

type PartnerChannelQueries struct {
	PartnerID   int    `json:"partner_id"`
	Name        string `json:"name"`
	DisplayName string `json:"display_name"`
	Code        string `json:"code"`
	Flag        string `json:"flag"`
	Type        int    `json:"type"`
	Ordering    int    `json:"ordering"`
	IsActive    bool   `json:"is_active"`
}

type PartnerChannel struct {
	PartnerID              int            `validate:"required,min=1" json:"partner_id"`
	Name                   string         `validate:"required,min=1" json:"name"`
	DisplayName            string         `validate:"required,min=1" json:"display_name"`
	Code                   string         `validate:"required,min=1" json:"code"`
	SecretKey              string         `validate:"required,min=1" json:"secret_key"`
	GrpCode                int            `validate:"required,min=1" json:"grp_code"`
	LogoUrl                sql.NullString `json:"logo_url"`
	MinimumAmount          int            `validate:"required,min=1" json:"minimum_amount"`
	IsManualPaymentAllowed bool           `validate:"required" json:"is_manual_payment_allowed"`
	NotifyUser             bool           `validate:"required" json:"notify_user"`
	DescriptionCode        string         `validate:"required,min=1" json:"description_code"`
	Flag                   string         `validate:"required,min=1" json:"flag"`
	Type                   int            `validate:"required,min=1" json:"type"`
	Ordering               int            `validate:"required,min=1" json:"ordering"`
	IsActive               bool           `validate:"required" json:"is_active"`
}

type UpdatePartnerChannel struct {
	Name        *string         ` json:"name"`
	DisplayName *string         `json:"display_name"`
	Type        *int            ` json:"type"`
	LogoUrl     *sql.NullString `json:"logo_url"`
	IsActive    *bool           `json:"is_active"`
	NotifyUser  *bool           `json:"notify_user"`
}
