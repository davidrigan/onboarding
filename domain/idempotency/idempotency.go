package domain

import (
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"context"
	"time"
)

// IdempotencyRepository interface to implement persistence for Idempotency domain
type IdempotencyRepository interface {
	Save(context.Context, *Idempotency) (int, error)
	Update(context.Context, *Idempotency) error
	FindByID(context.Context, int) (*Idempotency, error)
	FindByKeyAndRequestMethodAndRequestPath(context.Context, string, string, string) (*Idempotency, error)
}

// Idempotency domain
type Idempotency struct {
	ID             int
	IdempotencyKey string
	RequestPath    string
	RequestParam   string
	RequestMethod  string
	LockedAt       *time.Time
	ResponseCode   sql.NullInt64
	ResponseBody   sql.NullString
	Version        int
	CreatedAt      time.Time
	UpdatedAt      *time.Time
	DeletedAt      *time.Time
}

func (idk *Idempotency) SetResponse(responseCode int, responseBody string) {
	idk.ResponseCode = sql.GetNullInt64(int64(responseCode))
	idk.ResponseBody = sql.GetNullString(responseBody)
}

func (idk *Idempotency) SetRequest(key string, requestPath string,
	requestMethod string, requestParam string) {
	lockedAt := time.Now().UTC()
	idk.IdempotencyKey = key
	idk.RequestPath = requestPath
	idk.RequestParam = requestParam
	idk.RequestMethod = requestMethod
	idk.LockedAt = &lockedAt
	idk.Version = 0
}
