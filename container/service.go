package container

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"bitbucket.org/finaccelteam/onboarding/service/partnerChannel"
	"context"
)

func getPartnerService(ctx context.Context) (partner.Service, error) {
	repoPartner, err := getPartnerRepository(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Repository Partner")
	}
	servicePartner := partner.NewService(repoPartner)
	return servicePartner, nil
}

func getPartnerChannelService(ctx context.Context) (partnerChannel.Service, error) {
	repoPartnerChannel, err := getPartnerChannelRepository(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Repository Partner Channel")
	}
	repoPartner, err := getPartnerRepository(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Repository Partner")
	}
	servicePartnerChannel := partnerChannel.NewService(repoPartnerChannel, repoPartner)
	return servicePartnerChannel, nil
}
