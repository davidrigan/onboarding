package main

import (
	"bitbucket.org/finaccelteam/ms-common-go/config"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp/middleware"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/onboarding/broker"
	"bitbucket.org/finaccelteam/onboarding/container"
	"context"
	"github.com/gorilla/mux"
	"github.com/shopspring/decimal"
	"os"
)

func main() {
	ctx := context.Background()
	router := mux.NewRouter()
	decimal.MarshalJSONWithoutQuotes = true

	setConfig(ctx)

	// register middleware
	router.Use(middleware.ResponseTime)
	router.Use(middleware.CorrelationID)

	// initialize http container
	err := container.InitRoutes(ctx, router)
	if err != nil {
		logger.Fatal(ctx, "Error in initRoutes Error %v", ferror.StackTrace(err))
	}

	// create http server
	httpServer, err := container.InitServer(router)
	if err != nil {
		logger.Fatal(ctx, "Error while initialising server %s", err)
	}

	// Start http server
	logger.Info(ctx, "Starting http server...")
	err = httpServer.Start(ctx)
	if err != nil {
		logger.Error(ctx, "Error on starting up Http Server. %v", ferror.StackTrace(err))
	}
}

func setConfig(ctx context.Context) {
	//Get Config path if available in env var
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		configPath = "./config.yml"
	}

	registerConfigs()

	//load config
	if err := config.LoadConfig(configPath); err != nil {
		logger.Fatal(ctx, "Unable to load config. Error: %v.", err.Error())
	}

	//set service name in logger
	if err := logger.Initialise(logger.INFO, logger.JSONFormat, "ms-payment[Web]", "stg"); err != nil {
		logger.Fatal(ctx, "could not initialise logger")
	}
}

// kafka config - Two
func registerConfigs() {
	config.Register("server", container.GetServerConfig())
	config.Register("database", container.GetMySQLDBConfig())
	config.Register("kafka", broker.GetKafkaConfig())
}
