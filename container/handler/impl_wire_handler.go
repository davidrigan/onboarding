//go:build wireinject
// +build wireinject

package handler

import (
	"bitbucket.org/finaccelteam/onboarding/broker/subscriber"
	"bitbucket.org/finaccelteam/onboarding/container"
	"bitbucket.org/finaccelteam/onboarding/handler"
	"bitbucket.org/finaccelteam/onboarding/repository"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"bitbucket.org/finaccelteam/onboarding/service/partnerChannel"
	"context"
	"github.com/google/wire"
)

var (
	//partnerChannelCreationHandler handler.EventHandler
	//newIdempotenSet          = wire.NewSet(repository.NewIncomingEventRepository, idempotency.NewIdempotencyMiddleWare)
	newPartnerServSet        = wire.NewSet(repository.NewPartnerRepository, partner.NewService)
	newPartnerChannelServSet = wire.NewSet(repository.NewPartnerChannelRepository, partnerChannel.NewService)
)

func wirePartnerChannelHandler(ctx context.Context, isDlq bool, numberOfConsumers int) (handler.EventHandler, error) {
	wire.Build(subscriber.GetPartnerCreateKafkaOption, subscriber.GetKafkaEventSubscriber, handler.GetCreatePartnerChannelKafkaConfig, container.DatabaseSet, newPartnerServSet, newPartnerChannelServSet, handler.NewPartnerChannelHandler)
	return nil, nil
}

//func wireIdempotencyHandler(ctx context.Context) (*idempotency.IdempotencyMiddleWare, error) {
//	wire.Build(container.DatabaseSet, newIdempotenSet)
//	return nil, nil
//}
