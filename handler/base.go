package handler

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	ferror2 "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
	incomingEvent "bitbucket.org/finaccelteam/onboarding/domain/incomingEvent"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"context"
	"os"
	"sync"
)

var (
	eventHandlers []EventHandler
)

// EventHandler - Interface for handling events from subscribers
type EventHandler interface {
	StartSubscriber(*sync.WaitGroup, <-chan os.Signal, bool) error
}

// EventHandler - Interface for handling events from subscribers
type EventHandlerCloser interface {
	Close(context.Context) error
	Closed() <-chan struct{}
}

//RegisterEventHandler - Registers event handler
func RegisterEventHandler(handler EventHandler) {
	eventHandlers = append(eventHandlers, handler)
}

//IsEventProcessed - Function to check are event exists or not in incoming event db
func IsEventProcessed(ctx context.Context, eventID string, eventType int, incomingEventRepo incomingEvent.IncomingEventRepository) (bool, error) {
	incomingEvent, err := incomingEventRepo.FindByEventIDAndEventType(ctx, eventID, eventType)
	if err != nil {
		return false, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "IsEventProcessed FindByEventIDAndEventType repo")
	}
	if incomingEvent != nil {
		return true, nil
	}
	return false, nil
}

// buildErrorEvent - Wrapper for return message error based on Error Kind
func buildErrorEvent(err error) error {
	if ferror2.IsOfKind(err, errors.ErrUnprocessableKind) {
		return broker.NewBadMessageError(errors.ErrCodeInvalidDecode, err, "")
	}
	return err
}
