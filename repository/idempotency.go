package repository

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	domain "bitbucket.org/finaccelteam/onboarding/domain/idempotency"
	"context"
	sql2 "database/sql"
	"fmt"
	"strings"
)

var (
	allIdempotencyColNames = []string{
		idempotencyColID,
		idempotencyColIdempotencyKey,
		idempotencyColRequestPath,
		idempotencyColRequestParam,
		idempotencyColRequestMethod,
		idempotencyColLockedAt,
		idempotencyColResponseCode,
		idempotencyColResponseBody,
		idempotencyColVersion,
		idempotencyColCreatedAt,
		idempotencyColUpdatedAt,
		idempotencyColDeletedAt,
	}

	idempotencyColNamesForInsert = []string{
		idempotencyColIdempotencyKey,
		idempotencyColRequestPath,
		idempotencyColRequestParam,
		idempotencyColRequestMethod,
		idempotencyColLockedAt,
		idempotencyColResponseCode,
		idempotencyColResponseBody,
		idempotencyColVersion,
	}

	idempotencyColNamesForUpdate = []string{
		idempotencyColIdempotencyKey,
		idempotencyColRequestPath,
		idempotencyColRequestParam,
		idempotencyColRequestMethod,
		idempotencyColLockedAt,
		idempotencyColResponseCode,
		idempotencyColResponseBody,
		idempotencyColVersion,
	}

	idempotencyColNamesWithNullValue = []string{
		idempotencyColLockedAt,
		idempotencyColResponseCode,
		idempotencyColResponseBody,
	}
)

const (
	idempotencyTableName         = `idempotency`
	idempotencyColID             = `id`
	idempotencyColIdempotencyKey = `idempotency_key`
	idempotencyColRequestPath    = `request_path`
	idempotencyColRequestParam   = `request_param`
	idempotencyColRequestMethod  = `request_method`
	idempotencyColLockedAt       = `locked_at`
	idempotencyColResponseCode   = `response_code`
	idempotencyColResponseBody   = `response_body`
	idempotencyColVersion        = `version`
	idempotencyColCreatedAt      = `created_at`
	idempotencyColUpdatedAt      = `updated_at`
	idempotencyColDeletedAt      = `deleted_at`
)

// Idempotency - Implementation of the Idempotency repo interface in domain
type Idempotency struct {
	db sql.Queries
}

// NewIdempotencyRepository - Creates and returns an implementation of the Idempotency
func NewIdempotencyRepository(db sql.Queries) domain.IdempotencyRepository {
	return &Idempotency{
		db: db,
	}
}

func execIdempotencyKeyStatement(ctx context.Context, stmt *sql2.Stmt, idempotencyKey *domain.Idempotency, incrementVersion bool) (sql2.Result, error) {
	var version = idempotencyKey.Version

	if incrementVersion {
		version++
	}

	return stmt.ExecContext(ctx,
		idempotencyKey.IdempotencyKey,
		idempotencyKey.RequestPath,
		idempotencyKey.RequestParam,
		idempotencyKey.RequestMethod,
		idempotencyKey.LockedAt,
		idempotencyKey.ResponseCode,
		idempotencyKey.ResponseBody,
		version,
	)
}

// Save - Function that persists the given IdempotencyKey in the Idempotency_key table
func (i *Idempotency) Save(ctx context.Context, idempotencyKey *domain.Idempotency) (int, error) {
	colNames := strings.Join(idempotencyColNamesForInsert, ", ")
	placeholder := sql.GetQSPlaceholderForColumns(len(idempotencyColNamesForInsert))
	query := fmt.Sprintf("INSERT INTO %v(%v) VALUES (%v)", idempotencyTableName, colNames, placeholder)

	stmt, err := i.db.PrepareContext(ctx, query)
	if err != nil {
		return 0, ferror.Wrap(err, "prepare query")
	}

	defer func() {
		if err := stmt.Close(); err != nil {
			logger.Error(ctx, "Error in closing statement %v", err.Error())
		}
	}()

	res, err := execIdempotencyKeyStatement(ctx, stmt, idempotencyKey, false)
	if err != nil {
		return 0, ferror.Wrap(err, "exec idempotency key statement")
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, ferror.Wrap(err, "last insert id")
	}

	return int(id), nil
}

// Update - Function that updates the given IdempotencyKey in the Idempotency_key table
func (i *Idempotency) Update(ctx context.Context, idempotency *domain.Idempotency) error {
	placeholder := sql.GetUpdateQSPlaceholderForColumns(idempotencyColNamesForUpdate)
	query := fmt.Sprintf("UPDATE %v SET %v WHERE id = %d AND version = %d",
		idempotencyTableName, placeholder, idempotency.ID, idempotency.Version)

	stmt, err := i.db.PrepareContext(ctx, query)
	if err != nil {
		return ferror.Wrap(err, "prepare query")
	}

	defer func() {
		if err := stmt.Close(); err != nil {
			logger.Error(ctx, "Error in closing statement %v", err.Error())
		}
	}()

	res, err := execIdempotencyKeyStatement(ctx, stmt, idempotency, true)
	if err != nil {
		return ferror.Wrap(err, "exec idempotency key statement")
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return ferror.Wrap(err, "rows affected rslt")
	}
	if rowsAffected == 0 {
		return ferror.Wrap(err, "no rows affected")
	}

	return nil
}

func scanIdempotencyRow(row sql.Row) (*domain.Idempotency, error) {
	idempotencyKey := new(domain.Idempotency)

	if err := row.Scan(
		&idempotencyKey.ID,
		&idempotencyKey.IdempotencyKey,
		&idempotencyKey.RequestPath,
		&idempotencyKey.RequestParam,
		&idempotencyKey.RequestMethod,
		&idempotencyKey.LockedAt,
		&idempotencyKey.ResponseCode,
		&idempotencyKey.ResponseBody,
		&idempotencyKey.Version,
		&idempotencyKey.CreatedAt,
		&idempotencyKey.UpdatedAt,
		&idempotencyKey.DeletedAt,
	); err != nil {
		return nil, ferror.Wrap(err, "row scan")
	}
	return idempotencyKey, nil
}

// FindByID - Function that returns IdempotencyKey from Idempotency_key table for given ID
func (i *Idempotency) FindByID(ctx context.Context, id int) (*domain.Idempotency, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allIdempotencyColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, idempotencyTableName, idempotencyColID, idempotencyColDeletedAt)

	var idempotencyKey *domain.Idempotency
	scan := func(row sql.Row) error {
		tIdempotencyKey, err := scanIdempotencyRow(row)
		if err != nil {
			return ferror.Wrap(err, "scan idempotency row")
		}
		idempotencyKey = tIdempotencyKey
		return nil
	}

	if err := sql.Fetch(ctx, i.db, scan, query, id); err != nil {
		return nil, ferror.Wrap(err, "fetch")
	}

	return idempotencyKey, nil
}

// FindByKeyAndRequestMethodAndRequestPath - Function that returns IdempotencyKey from Idempotency_key table for given key
func (i *Idempotency) FindByKeyAndRequestMethodAndRequestPath(ctx context.Context, key string, method string, path string) (*domain.Idempotency, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allIdempotencyColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v = ? AND %v = ? AND %v IS NULL",
		columnNames,
		idempotencyTableName,
		idempotencyColIdempotencyKey,
		idempotencyColRequestMethod,
		idempotencyColRequestPath,
		idempotencyColDeletedAt)

	var idempotency *domain.Idempotency
	scan := func(row sql.Row) error {
		tIdempotency, err := scanIdempotencyRow(row)
		if err != nil {
			return ferror.Wrap(err, "scan idempotency row")
		}
		idempotency = tIdempotency
		return nil
	}

	if err := sql.Fetch(ctx, i.db, scan, query, key, method, path); err != nil {
		return nil, ferror.Wrap(err, "fetch")
	}

	return idempotency, nil
}
