package response

import "bitbucket.org/finaccelteam/ms-common-go/sql"

// 5
type PartnerChannel struct {
	ID                     int            `json:"id"`
	PartnerID              int            `json:"partner_id"`
	Name                   string         `json:"name"`
	DisplayName            string         `json:"display_name"`
	Code                   string         `json:"code"`
	SecretKey              string         `json:"secret_key"`
	GrpCode                int            `json:"grp_code"`
	Flag                   string         `json:"flag"`
	Type                   int            `json:"type"`
	LogoUrl                sql.NullString `json:"logo_url"`
	MinimumAmount          int            `json:"minimum_amount"`
	IsManualPaymentAllowed bool           `json:"is_manual_payment_allowed"`
	Ordering               int            `json:"ordering"`
	IsActive               bool           `json:"is_active"`
	NotifyUser             bool           `json:"notify_user"`
	DescriptionCode        string         `json:"description_code"`
}
