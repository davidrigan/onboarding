package partnerChannel

import (
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"context"
	"time"
)

// Repository interface to implement persistence for PartnerChannel domain
type Repository interface {
	Save(context.Context, *PartnerChannel) (int, error)
	Update(ctx context.Context, partnerChannelParam *PartnerChannel) error
	FindByID(context.Context, int) (*PartnerChannel, error)
	FindByEntityIDAndActiveAndMinAmountGTOrderBYOrdering(context.Context, int, int) ([]*PartnerChannel, error)
	FindByCode(context.Context, string) (*PartnerChannel, error)
	FindByPartnerID(ctx context.Context, partnerID int) (*PartnerChannel, error)
}

// PartnerChannel domain
type PartnerChannel struct {
	ID                     int
	PartnerID              int
	Name                   string
	DisplayName            string
	Code                   string
	SecretKey              string
	GrpCode                int
	Flag                   string
	Type                   int
	LogoUrl                sql.NullString
	MinimumAmount          int
	IsManualPaymentAllowed bool
	Ordering               int
	IsActive               bool
	NotifyUser             bool
	DescriptionCode        string
	Timezone               string
	Version                int
	CreatedAt              time.Time
	CreatedBy              string
	UpdatedAt              *time.Time
	UpdatedBy              string
	DeletedAt              *time.Time
	DeletedBy              sql.NullString
}
