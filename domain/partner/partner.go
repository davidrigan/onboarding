package partner

import (
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"context"
	"time"
)

// Repository interface to implement persistence for Partner domain
type Repository interface {
	Save(context.Context, *Partner) (int, error)
	Update(context.Context, *Partner) error
	FindByID(context.Context, int) (*Partner, error)
	Get(ctx context.Context) ([]*Partner, error)
	FindByMerchantID(context.Context, string) ([]*Partner, error)
	FindByGatewayType(ctx context.Context, gatewayType int) ([]*Partner, error)
	FindByMerchantIDAndGatewayType(ctx context.Context, merchantID string, gatewayType int) ([]*Partner, error)
}

// Partner domain
type Partner struct {
	ID          int
	Name        string
	MerchantID  string
	GatewayType int
	IsActive    bool
	EntityID    int
	Timezone    string
	Version     int
	CreatedAt   time.Time
	CreatedBy   string
	UpdatedAt   *time.Time
	UpdatedBy   string
	DeletedAt   *time.Time
	DeletedBy   sql.NullString
}
