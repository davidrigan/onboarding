package enum

//IncomingEventType - Enum for IncomingEventType
var IncomingEventType = struct {
	Partner        int
	PartnerChannel int
}{
	Partner:        10,
	PartnerChannel: 20,
}

//OutgoingEvent - Enum for OutgoingEvent
var OutgoingEvent = struct {
	PartnerCreatedEvent        int
	PartnerChannelCreatedEvent int
}{
	PartnerCreatedEvent:        10,
	PartnerChannelCreatedEvent: 20,
}