package container

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker/kafka"
	commonController "bitbucket.org/finaccelteam/ms-common-go/controller"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/onboarding/broker/publisher"
	"bitbucket.org/finaccelteam/onboarding/controller"
	"bitbucket.org/finaccelteam/onboarding/middleware"
	"context"
	"github.com/gorilla/mux"
)

// InitRoutes - Register all Controller to main program, so we can call the controller
func InitRoutes(ctx context.Context, router *mux.Router) error {
	GetHomeController(router)
	_, err := GetPartnerController(ctx, router)
	if err != nil {
		return err
	}
	_, err = GetPartnerChannelController(ctx, router)
	if err != nil {
		return err
	}
	return nil
}

//GetHomeController - initializes home controller
func GetHomeController(router *mux.Router) commonController.Controller {
	return controller.NewHome(router)
}

// GetPartnerController - initializes partner controller
func GetPartnerController(ctx context.Context, router *mux.Router) (commonController.Controller, error) {
	tx, err := GetTransactional(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Transaction DB error")
	}
	kafkaPublisher, err := publisher.GetKafkaEventPublisher()
	if err != nil {
		return nil, ferror.Wrap(err, "Kafka Publisher error")
	}
	partnerPublisher := kafkaPublisher.(*kafka.Publisher)
	service, err := getPartnerService(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Service Partner")
	}
	idempotency, err := GetIdempotency(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Idempotency Repo")
	}
	return controller.NewPartnerController(router, service, tx, partnerPublisher, idempotency), nil
}

// GetPartnerChannelController - initialize partner channel controller
func GetPartnerChannelController(ctx context.Context, router *mux.Router) (commonController.Controller, error) {
	tx, err := GetTransactional(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "DB Error")
	}
	service, err := getPartnerChannelService(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Service Partner Channel")
	}
	idempotency, err := GetIdempotency(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Idempotency Repo")
	}
	return controller.NewPartnerChannelController(router, service, tx, idempotency), nil
}

func GetIdempotency(ctx context.Context) (middleware.IdempotencyMiddleWare, error) {
	tx, err := GetTransactional(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "DB Error")
	}
	repo, err := getIdempotencyRepository(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Repository Error")
	}
	return middleware.NewIdempotencyMiddleWare(tx, repo)
}
