package repository

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	outgoingEventDomain "bitbucket.org/finaccelteam/onboarding/domain/outgoingEvent"
	"context"
	dsql "database/sql"
	"fmt"
	"strings"
	"time"
)

var (
	allOutgoingEventColNames = []string{
		outgoingEventColID,
		outgoingEventColLoanID,
		outgoingEventColEventType,
		outgoingEventColEventID,
		outgoingEventColIncomingRequestID,
		outgoingEventColPayload,
		outgoingEventColDispatchedAt,
		outgoingEventColIsDispatched,
		outgoingEventColVersion,
		outgoingEventColCreatedAt,
		outgoingEventColUpdatedAt,
		outgoingEventColDeletedAt,
	}

	outgoingEventColNamesForInsert = []string{
		outgoingEventColLoanID,
		outgoingEventColEventType,
		outgoingEventColEventID,
		outgoingEventColIncomingRequestID,
		outgoingEventColPayload,
		outgoingEventColDispatchedAt,
		outgoingEventColIsDispatched,
		outgoingEventColVersion,
	}

	outgoingEventColNamesForUpdate = []string{
		outgoingEventColDispatchedAt,
		outgoingEventColIsDispatched,
		outgoingEventColVersion,
	}
	outgoingEventRepo outgoingEventDomain.Repository
)

const (
	outgoingEventTableName            = `outgoing_event`
	outgoingEventColID                = `id`
	outgoingEventColLoanID            = `partition_key`
	outgoingEventColEventType         = `event_type`
	outgoingEventColEventID           = `event_id`
	outgoingEventColIncomingRequestID = `incoming_request_id`
	outgoingEventColPayload           = `payload`
	outgoingEventColDispatchedAt      = `dispatched_at`
	outgoingEventColIsDispatched      = `is_dispatched`
	outgoingEventColVersion           = `version`
	outgoingEventColCreatedAt         = `created_at`
	outgoingEventColUpdatedAt         = `updated_at`
	outgoingEventColDeletedAt         = `deleted_at`
)

//OutgoingEvent - Implementation of the OutgoingEvent repo interface in domain
type OutgoingEvent struct {
	db sql.Queries
}

func execOutgoingEventStatement(stmt *dsql.Stmt, event *outgoingEventDomain.OutgoingEvent, saveAll bool, incrementVersion bool) (dsql.Result, error) {
	args := make([]interface{}, 0)
	var version = event.Version

	if incrementVersion {
		version++
	}

	partitionKey := sql.GetNullInt64FromIntReference(event.PartitionKey)

	if saveAll {
		args = append(
			args,
			partitionKey,
			event.EventType,
			event.EventID,
			event.IncomingRequestID,
			event.Payload,
		)
	}
	args = append(
		args,
		event.DispatchedAt,
		event.IsDispatched,
		version,
	)
	return stmt.Exec(args...)
}

//Save - Function that persists the given event in the outgoing_event table
func (ab *OutgoingEvent) Save(ctx context.Context, event *outgoingEventDomain.OutgoingEvent) (int, error) {
	colNames := strings.Join(outgoingEventColNamesForInsert, ", ")
	placeholder := sql.GetQSPlaceholderForColumns(len(outgoingEventColNamesForInsert))
	query := fmt.Sprintf("INSERT INTO %v(%v) VALUES (%v)", outgoingEventTableName, colNames, placeholder)

	stmt, stmtErr := ab.db.PrepareContext(ctx, query)
	if stmtErr != nil {
		return 0, ferror.Wrap(stmtErr, "prepare stmt")
	}

	defer stmt.Close()

	res, execErr := execOutgoingEventStatement(stmt, event, true, false)
	if execErr != nil {
		return 0, ferror.Wrap(execErr, "execute stmt")
	}

	id, idErr := res.LastInsertId()
	if idErr != nil {
		return 0, ferror.Wrap(idErr, "last insertID")
	}

	return int(id), nil
}

//Update - Function that updates the given Event in the outgoing_event table
func (ab *OutgoingEvent) Update(ctx context.Context, outgoingEvent *outgoingEventDomain.OutgoingEvent) error {
	placeholder := sql.GetUpdateQSPlaceholderForColumns(outgoingEventColNamesForUpdate)
	query := fmt.Sprintf("UPDATE %v SET %v WHERE id = %d AND version = %d",
		outgoingEventTableName, placeholder, outgoingEvent.ID, outgoingEvent.Version)
	stmt, stmtErr := ab.db.PrepareContext(ctx, query)
	if stmtErr != nil {
		return ferror.Wrap(stmtErr, "prepare stmt")
	}
	defer stmt.Close()

	res, execErr := execOutgoingEventStatement(stmt, outgoingEvent, false, true)

	if execErr != nil {
		return ferror.Wrap(execErr, "execute stmt")
	}

	rowsAffected, _ := res.RowsAffected()
	if rowsAffected == 0 {
		errMsg := fmt.Sprintf(
			"Version is mismatched while updating the row for outgoing event id %d. Current version %d",
			outgoingEvent.ID, outgoingEvent.Version)
		return ferror.E(errMsg)
	}

	return nil
}

func scanOutgoingEventRow(row sql.Row) (*outgoingEventDomain.OutgoingEvent, error) {
	event := new(outgoingEventDomain.OutgoingEvent)

	if err := row.Scan(
		&event.ID,
		&event.PartitionKey,
		&event.EventType,
		&event.EventID,
		&event.IncomingRequestID,
		&event.Payload,
		&event.DispatchedAt,
		&event.IsDispatched,
		&event.Version,
		&event.CreatedAt,
		&event.UpdatedAt,
		&event.DeletedAt,
	); err != nil {
		return nil, ferror.Wrap(err, "scan")
	}

	return event, nil
}

//FindByID - Function that returns Event from outgoing_event table for given ID
func (ab *OutgoingEvent) FindByID(ctx context.Context, id int) (*outgoingEventDomain.OutgoingEvent, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allOutgoingEventColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, outgoingEventTableName, outgoingEventColID, outgoingEventColDeletedAt)
	var event *outgoingEventDomain.OutgoingEvent
	scan := func(row sql.Row) error {
		tEvent, err := scanOutgoingEventRow(row)
		if err != nil {
			return err
		}

		event = tEvent
		return nil
	}
	if err := sql.Fetch(ctx, ab.db, scan, query, id); err != nil {
		return nil, ferror.Wrap(err, "fetch")
	}

	return event, nil
}

//FindByOutgoingEventType - Function that returns undispatched events from outgoing_event table for given event type
func (ab *OutgoingEvent) FindByOutgoingEventType(ctx context.Context, eventType int) ([]*outgoingEventDomain.OutgoingEvent, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allOutgoingEventColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v = ? AND %v IS NULL", columnNames, outgoingEventTableName, outgoingEventColEventType,
		outgoingEventColIsDispatched, outgoingEventColDeletedAt)

	events := make([]*outgoingEventDomain.OutgoingEvent, 0)
	scan := func(row sql.Row) error {
		tEvent, err := scanOutgoingEventRow(row)
		if err != nil {
			return err
		}

		events = append(events, tEvent)
		return nil
	}
	if err := sql.Fetch(ctx, ab.db, scan, query, eventType, false); err != nil {
		return nil, ferror.Wrap(err, "fetch")
	}

	return events, nil
}

// FindUnpublishedOutgoingEvents - Function that returns loans event with isPublished equal to False from outgoing_event table
func (ab *OutgoingEvent) FindUnpublishedOutgoingEvents(ctx context.Context) ([]*outgoingEventDomain.OutgoingEvent, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allOutgoingEventColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, outgoingEventTableName,
		outgoingEventColIsDispatched, outgoingEventColDeletedAt)
	events := make([]*outgoingEventDomain.OutgoingEvent, 0)
	scan := func(row sql.Row) error {
		event, err := scanOutgoingEventRow(row)
		if err != nil {
			return err
		}
		events = append(events, event)

		return nil
	}
	if err := sql.Fetch(ctx, ab.db, scan, query, false); err != nil {
		return nil, ferror.Wrap(err, "fetch")
	}

	return events, nil
}

//FindByEventTypeAndIncomingRequestIDAndIsDispatched - Function that returns undispatched events from outgoing_event table for given event type
func (ab *OutgoingEvent) FindByEventTypeAndIncomingRequestIDAndIsDispatched(
	ctx context.Context,
	eventType int,
	incomingRequestID string,
	isDispatched bool,
) ([]*outgoingEventDomain.OutgoingEvent, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allOutgoingEventColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v = ? AND %v = ? AND %v IS NULL", columnNames,
		outgoingEventTableName,
		outgoingEventColIncomingRequestID,
		outgoingEventColIsDispatched,
		outgoingEventColEventType,
		outgoingEventColDeletedAt)

	events := make([]*outgoingEventDomain.OutgoingEvent, 0)
	scan := func(row sql.Row) error {
		tEvent, err := scanOutgoingEventRow(row)
		if err != nil {
			return err
		}

		events = append(events, tEvent)
		return nil
	}
	if err := sql.Fetch(ctx, ab.db, scan, query, incomingRequestID, isDispatched, eventType); err != nil {
		return nil, ferror.Wrap(err, "fetch")
	}

	return events, nil
}

//FindByIncomingRequestIDAndIsDispatched - Function that returns undispatched events from outgoing_event table for given event type
func (ab *OutgoingEvent) FindByIncomingRequestIDAndIsDispatched(ctx context.Context, incomingRequestID string,
	isDispatched bool) ([]*outgoingEventDomain.OutgoingEvent, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allOutgoingEventColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v = ?  AND %v IS NULL", columnNames,
		outgoingEventTableName,
		outgoingEventColIsDispatched,
		outgoingEventColIncomingRequestID,
		outgoingEventColDeletedAt)

	events := make([]*outgoingEventDomain.OutgoingEvent, 0)
	scan := func(row sql.Row) error {
		tEvent, err := scanOutgoingEventRow(row)
		if err != nil {
			return err
		}

		events = append(events, tEvent)
		return nil
	}
	if err := sql.Fetch(ctx, ab.db, scan, query, isDispatched, incomingRequestID); err != nil {
		return nil, ferror.Wrap(err, "fetch")
	}

	return events, nil
}

// HardDeleteByCreatedAt - hard delete records older than date
func (ab *OutgoingEvent) HardDeleteByCreatedAt(ctx context.Context, date time.Time, limit int) (int, error) {
	query := fmt.Sprintf("DELETE FROM %v WHERE %v < ? LIMIT %v", outgoingEventTableName, outgoingEventColCreatedAt, limit)
	stmt, err := ab.db.PrepareContext(ctx, query)
	if err != nil {
		return 0, ferror.Wrap(err, "prepare stmt")
	}

	defer func() {
		if err := stmt.Close(); err != nil {
			logger.Error(ctx, "Error in closing statement %v", err.Error())
		}
	}()

	res, err := stmt.ExecContext(ctx, date)
	if err != nil {
		return 0, ferror.Wrap(err, "execute stmt")
	}

	rowsDeleted, err := res.RowsAffected()
	if err != nil {
		return 0, ferror.Wrap(err, "rows affected")
	}

	return int(rowsDeleted), nil
}

//NewOutgoingEventRepository - Creates and returns an implementation of the OutgoingEvent
func NewOutgoingEventRepository(db sql.Queries) (outgoingEventDomain.Repository, error) {
	if db == nil {
		return nil, ferror.E("db object is nil")
	}
	return &OutgoingEvent{
		db: db,
	}, nil
}
