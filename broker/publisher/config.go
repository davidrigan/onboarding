package publisher

var (
	partnerKafkaPublisherConfig        KafkaPublisherConfig
	partnerChannelKafkaPublisherConfig KafkaPublisherConfig
)

//KafkaPublisherConfig - Config for kafka publisher
type KafkaPublisherConfig struct {
	TopicName string `mapstructure:"topicName"`
}

//LoadInto - returns the pointer to the struct
func (c *KafkaPublisherConfig) LoadInto() interface{} {
	return &c
}

//GetPartnerKafkaPublisherConfig - returns a pointer to the var lenderDisbursementKafkaPublisherConfig
func GetPartnerKafkaPublisherConfig() *KafkaPublisherConfig {
	return &partnerKafkaPublisherConfig
}

//GetPartnerKafkaPublisherConfig - returns a pointer to the var lenderDisbursementKafkaPublisherConfig
func GetPartnerChannelKafkaPublisherConfig() *KafkaPublisherConfig {
	return &partnerChannelKafkaPublisherConfig
}
