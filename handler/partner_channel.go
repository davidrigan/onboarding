package handler

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"bitbucket.org/finaccelteam/onboarding/service/partnerChannel"
	"context"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"os"
	"sync"
)

type partnerChannelHandler struct {
	subscriber            broker.Subscriber
	servicePartner        partner.Service
	servicePartnerChannel partnerChannel.Service
	validate              *validator.Validate
}

func (h *partnerChannelHandler) StartSubscriber(wg *sync.WaitGroup, exit <-chan os.Signal, isDLQueue bool) error {
	ctx := context.Background()
	err := h.subscriber.Subscribe(ctx, h.partnerChannelCreateHandler)
	if err != nil {
		return err
	}
	return nil
}

func (h *partnerChannelHandler) partnerChannelCreateHandler(ctx context.Context, m broker.PublishedMessage) error {
	var err error
	req := &request.PartnerChannel{}

	logger.Info(ctx, "Unmarshall data partner: %v", string(m.Message()))
	if err = json.Unmarshal(m.Message(), req); err != nil {
		return errors.ErrKindUnprocessableWithCause(errors.ErrCodeInvalidUnmarshall, err, "")
	}

	logger.Info(ctx, "Validating data : %v", req)
	if err = h.validate.Struct(req); err != nil {
		return errors.ErrKindUnprocessableWithCause(errors.ErrCodeInvalidValidateStruct, err, "")
	}

	partnerChannelID, err := h.servicePartnerChannel.CreatePartnerChannel(ctx, req)
	if err != nil {
		return ferror.Wrap(err, "CreatePartnerChannel")
	}

	logger.Info(ctx, "Successfully consume message for partnerChannelID: %v", partnerChannelID)
	return nil
}

func NewPartnerChannelHandler(subscriber broker.Subscriber, servicePartner partner.Service, servicePartnerChannel partnerChannel.Service) EventHandler {
	return &partnerChannelHandler{
		subscriber:            subscriber,
		servicePartner:        servicePartner,
		servicePartnerChannel: servicePartnerChannel,
		validate:              validator.New(),
	}
}
