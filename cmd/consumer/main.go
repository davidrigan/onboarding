package main

import (
	"bitbucket.org/finaccelteam/ms-common-go/config"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/onboarding/broker"
	"bitbucket.org/finaccelteam/onboarding/broker/publisher"
	"bitbucket.org/finaccelteam/onboarding/container"
	"bitbucket.org/finaccelteam/onboarding/enum"
	"bitbucket.org/finaccelteam/onboarding/handler"
	"context"
	"flag"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	ctx := context.Background()

	// define the value of configuration
	var err error
	var consumer string
	var eventHandler handler.EventHandler
	var isDLQueue bool // when it is true, it means we have condition of messages in dlq want to consume
	var numberOfConsumer int

	// Set flags received on command line
	flag.StringVar(&consumer, "consumer", enum.Processes.Partner, "Type of consumer to run")
	flag.BoolVar(&isDLQueue, "dlq", false, "Run this consumer on Dead Letter Queue")
	flag.IntVar(&numberOfConsumer, "numberOfConsumer", 1, "Number of consumer to run on Dead Letter Queue")
	flag.Parse()

	switch consumer {
	case enum.Processes.Partner:
		setConfig(ctx, consumer)
		eventHandler, err = container.PartnerHandler(ctx, isDLQueue, numberOfConsumer)
		if err != nil {
			logger.Fatal(ctx, "Error creating partner handler. %v", err.Error())
		}
	case enum.Processes.PartnerChannel:
		setConfig(ctx, consumer)
		eventHandler, err = container.PartnerChannelHandler(ctx, isDLQueue, numberOfConsumer)
		if err != nil {
			logger.Fatal(ctx, "Error creating partner channel handler. %v", err.Error())
		}
	default:
		logger.Fatal(ctx, "Invalid consumer flag. Please provide a valid consumer flag.")
	}

	defer func() {
		if err := container.CloseConnections(); err != nil {
			logger.Fatal(ctx, "failed to close connections of container : ", err)
		}
	}()

	// This WaitGroup is used to wait for all the goroutines launched here to finish
	var wg sync.WaitGroup
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	// subscribe to queue
	if err := eventHandler.StartSubscriber(&wg, exit, isDLQueue); err != nil {
		logger.Fatal(ctx, "Error subscribing to %v queue. Error: %v", consumer, err.Error())
	}

	switch v := eventHandler.(type) {
	case handler.EventHandlerCloser:
		{
			closed := v.Closed()

			select {
			case <-exit:
				logger.Info(ctx, "Shutting down %v consumer....", consumer)
				err := v.Close(context.Background())
				if err != nil {
					logger.Error(ctx, "Error while closing subscriber")
					break
				}
				select {
				case <-closed:
					logger.Info(ctx, "Subscriber closed successfully")
				case <-time.After(30 * time.Second):
					logger.Info(ctx, "Subscriber closed forcefully")
				}
			case <-closed:
				{
					logger.Info(ctx, "Subscriber closed successfully")
				}
			}
		}
	default:
		{
			<-exit
			logger.Info(ctx, "Subscriber closed successfully")
			close(exit)
			// Wait for worker to shut down
			wg.Wait()
			// Block and exit on interrupt
			logger.Info(ctx, "Subscriber closed successfully")
		}
	}

	logger.Info(ctx, "Exiting %v consumer", consumer)
	os.Exit(0)
}

func setConfig(ctx context.Context, consumer string) {
	//Get Config path if available in env var
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		configPath = "./config.yml"
	}

	switch consumer {
	case enum.Processes.Partner:
		registerConfigPartner()
	case enum.Processes.PartnerChannel:
		registerConfigPartnerChannel()
	default:
		logger.Fatal(ctx, "Invalid consumer flag. Please provide a valid consumer flag.")
	}

	registerConfig()

	if err := config.LoadConfig(configPath); err != nil {
		logger.Fatal(ctx, "failed to load configs : ", err)
	}

	// set config for publish message
	publisher.SetKafkaPublishedEventToTopicMap(ctx, consumer)

	//set service name in logger
	if err := logger.Initialise(logger.INFO, logger.JSONFormat, "onboarding[Consumer]", "stg"); err != nil {
		logger.Fatal(ctx, "could not initialise logger")
	}
}

func registerConfig() {
	config.Register("kafka", broker.GetKafkaConfig())
	config.Register("database", container.GetMySQLDBConfig())
}

// register consumer config
func registerConfigPartner() {
	config.Register("create_partner_consumer", handler.GetCreatePartnerKafkaConfig())
	config.Register("create_partner_publisher", publisher.GetPartnerKafkaPublisherConfig())

}

func registerConfigPartnerChannel() {
	config.Register("create_partner_channel_consumer", handler.GetCreatePartnerChannelKafkaConfig())
	config.Register("create_partner_channel_publisher", publisher.GetPartnerChannelKafkaPublisherConfig())
}
