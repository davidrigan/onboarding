package subscriber

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/broker/kafka"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	commonBroker "bitbucket.org/finaccelteam/onboarding/broker"
)

// kafka consumer config
func GetKafkaEventSubscriber(consumerHandlerConfig *ConsumerConfig, isDlq bool, numberOfConsumers int, option *Option) (broker.Subscriber, error) {
	var disableSSLVerificationOption kafka.SubscriberOption
	var offsetOption kafka.SubscriberOption
	var subscriberOptions []kafka.SubscriberOption

	maxPollIntervalInSec := option.MaxPoolIntervalInSec
	autoOffsetReset := option.AutoOffsetReset

	//consumer config
	config := commonBroker.GetKafkaConfig()
	consumerConfig := GetKafkaConsumerConfig(consumerHandlerConfig)

	//dlq subs option
	dlqOption := kafka.SubscriberIsDlqOption(isDlq)
	subscriberOptions = append(subscriberOptions, dlqOption)

	//sasl subs option -> secure connections like HTTPS is essential between client applications and Kafka Server in kafka protocol
	//saslConfig := commonBroker.GetSASLKafkaConfig(config.SASLConfig)
	//sslOptions := kafka.SubscriberSASLConfigOption(saslConfig)
	//subscriberOptions = append(subscriberOptions, sslOptions)

	//maxPollIntervalInSec subs option -> time interval when pull the message
	if maxPollIntervalInSec <= 0 {
		maxPollIntervalInSec = standardKafkaPollInterval
	}
	pollOption := kafka.SubscriberMaxPollIntervalInSecOption(maxPollIntervalInSec)
	subscriberOptions = append(subscriberOptions, pollOption)

	//ssl subs option
	if config.DisableSSLVerification != nil {
		disableSSLVerificationOption = kafka.SubscriberDisableSSLVerificationOption(config.DisableSSLVerification)
		subscriberOptions = append(subscriberOptions, disableSSLVerificationOption)
	}

	//auto offset reset subs option
	offsetOption = kafka.SubscriberAutoOffsetResetOption(autoOffsetReset)

	if isDlq {
		consumerConfig.NumberOfConsumerWorkers = numberOfConsumers
		offsetOption = kafka.SubscriberAutoOffsetResetOption(kafka.PartitionOffsetEarliest)
	}

	subscriberOptions = append(subscriberOptions, offsetOption)

	subscriberOptions = append(subscriberOptions)

	sub, err := kafka.NewSubscriber(config.BootstrapServer, consumerConfig, subscriberOptions...)
	if err != nil {
		return nil, ferror.Wrap(err, "new kafka subscriber")
	}

	return sub, nil
}
