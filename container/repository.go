package container

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	idempotecy "bitbucket.org/finaccelteam/onboarding/domain/idempotency"
	incomingEvent "bitbucket.org/finaccelteam/onboarding/domain/incomingEvent"
	outgoingEvent "bitbucket.org/finaccelteam/onboarding/domain/outgoingEvent"
	"bitbucket.org/finaccelteam/onboarding/domain/partner"
	"bitbucket.org/finaccelteam/onboarding/domain/partnerChannel"
	"bitbucket.org/finaccelteam/onboarding/repository"
	"context"
)

func getPartnerRepository(ctx context.Context) (partner.Repository, error) {
	dbtx, err := getDBTX(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "DB error")
	}
	repoPartner := repository.NewPartnerRepository(dbtx)
	return repoPartner, nil
}

func getPartnerChannelRepository(ctx context.Context) (partnerChannel.Repository, error) {
	dbtx, err := getDBTX(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "DB error")
	}
	repoPartnerChannel := repository.NewPartnerChannelRepository(dbtx)
	return repoPartnerChannel, nil
}

func getIdempotencyRepository(ctx context.Context) (idempotecy.IdempotencyRepository, error) {
	dbtx, err := getDBTX(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "DB error")
	}
	repoIdempotency := repository.NewIdempotencyRepository(dbtx)
	return repoIdempotency, nil
}

func getOutGoingEventRepository(ctx context.Context) (outgoingEvent.Repository, error) {
	dbtx, err := getDBTX(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "DB error")
	}
	repoOutgoingEvent, err := repository.NewOutgoingEventRepository(dbtx)
	if err != nil {
		return nil, ferror.Wrap(err, "Outgoing Event")
	}
	return repoOutgoingEvent, nil
}

func getIncomingEventRepository(ctx context.Context) (incomingEvent.IncomingEventRepository, error) {
	dbtx, err := getDBTX(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "DB error")
	}
	repoIncomingEvent := repository.NewIncomingEventRepository(dbtx)
	return repoIncomingEvent, nil
}
