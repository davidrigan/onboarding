package subscriber

var partnerCreateKafkaOption Option

type Option struct {
	MaxPoolIntervalInSec int
	AutoOffsetReset      string
}

func GetPartnerCreateKafkaOption() *Option {
	partnerCreateKafkaOption = Option{
		MaxPoolIntervalInSec: standardKafkaPollInterval,
		AutoOffsetReset:      standardkafkaPartitionOffset,
	}
	return &partnerCreateKafkaOption
}
