package repository

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	domain "bitbucket.org/finaccelteam/onboarding/domain/partnerChannel"
	"context"
	"fmt"
	"strings"
)

var (
	allPartnerChannelColNames = []string{
		partnerChannelColID,
		partnerChannelColPartnerID,
		partnerChannelColName,
		partnerChannelColDisplayName,
		partnerChannelColCode,
		partnerChannelColSecretKey,
		partnerChannelColGrpcode,
		partnerChannelColFlag,
		partnerChannelColType,
		partnerChannelColLogoUrl,
		partnerChannelColMinimumAmount,
		partnerChannelColIsManualPaymentAllowed,
		partnerChannelColOrdering,
		partnerChannelColIsActive,
		partnerChannelColNotifyUser,
		partnerChannelColDescriptionCode,
		partnerChannelColTimezone,
		partnerChannelColVersion,
		partnerChannelColCreatedAt,
		partnerChannelColCreatedBy,
		partnerChannelColUpdatedAt,
		partnerChannelColUpdatedBy,
		partnerChannelColDeletedAt,
		partnerChannelColDeletedBy,
	}

	partnerChannelColNamesForInsert = []string{
		partnerChannelColPartnerID,
		partnerChannelColName,
		partnerChannelColDisplayName,
		partnerChannelColCode,
		partnerChannelColSecretKey,
		partnerChannelColGrpcode,
		partnerChannelColFlag,
		partnerChannelColType,
		partnerChannelColLogoUrl,
		partnerChannelColMinimumAmount,
		partnerChannelColIsManualPaymentAllowed,
		partnerChannelColOrdering,
		partnerChannelColIsActive,
		partnerChannelColNotifyUser,
		partnerChannelColDescriptionCode,
		partnerChannelColTimezone,
		partnerChannelColVersion,
		partnerChannelColCreatedBy,
		partnerChannelColUpdatedBy,
		partnerChannelColDeletedBy,
	}

	partnerChannelColNamesForUpdate = []string{
		partnerChannelColName,
		partnerChannelColDisplayName,
		partnerChannelColType,
		partnerChannelColLogoUrl,
		partnerChannelColIsActive,
		partnerChannelColNotifyUser,
		partnerChannelColVersion,
		partnerChannelColUpdatedBy,
	}

	partnerChannelColNamesWithNullValue = []string{
		partnerChannelColLogoUrl,
		partnerChannelColDeletedBy,
	}
)

const (
	partnerChannelTableName                 = `partner_channel`
	partnerChannelColID                     = `id`
	partnerChannelColPartnerID              = `partner_id`
	partnerChannelColName                   = `name`
	partnerChannelColDisplayName            = `display_name`
	partnerChannelColCode                   = `code`
	partnerChannelColSecretKey              = `secret_key`
	partnerChannelColGrpcode                = `grpCode`
	partnerChannelColFlag                   = `flag`
	partnerChannelColType                   = `type`
	partnerChannelColLogoUrl                = `logo_url`
	partnerChannelColMinimumAmount          = `minimum_amount`
	partnerChannelColIsManualPaymentAllowed = `is_manual_payment_allowed`
	partnerChannelColOrdering               = `ordering`
	partnerChannelColIsActive               = `is_active`
	partnerChannelColNotifyUser             = `notify_user`
	partnerChannelColDescriptionCode        = `description_code`
	partnerChannelColTimezone               = `timezone`
	partnerChannelColVersion                = `version`
	partnerChannelColCreatedAt              = `created_at`
	partnerChannelColCreatedBy              = `created_by`
	partnerChannelColUpdatedAt              = `updated_at`
	partnerChannelColUpdatedBy              = `updated_by`
	partnerChannelColDeletedAt              = `deleted_at`
	partnerChannelColDeletedBy              = `deleted_by`
)

var partnerChannelJoinCols = fmt.Sprintf("%v.%v= %v.%v", partnerTableName, partnerColID, partnerChannelTableName, partnerChannelColPartnerID)

// PartnerChannel - Implementation of the PartnerChannel repo interface in domain
type PartnerChannel struct {
	db sql.Queries
}

// NewPartnerChannelRepository - Creates and returns an implementation of the PartnerChannel
func NewPartnerChannelRepository(db sql.Queries) domain.Repository {
	return &PartnerChannel{
		db: db,
	}
}

// Save - Function that persists the given PartnerChannel in the partner_channel table
func (pc *PartnerChannel) Save(ctx context.Context, partnerChannel *domain.PartnerChannel) (int, error) {
	colNames := strings.Join(partnerChannelColNamesForInsert, ", ")
	placeholder := sql.GetQSPlaceholderForColumns(len(partnerChannelColNamesForInsert))
	query := fmt.Sprintf("INSERT INTO %v(%v) VALUES (%v)", partnerChannelTableName, colNames, placeholder)

	stmt, err := pc.db.PrepareContext(ctx, query)
	if err != nil {
		return 0, ferror.Wrap(err, "prepare query")
	}

	defer func() {
		if err := stmt.Close(); err != nil {
			logger.Error(ctx, "Error in closing statement %v", err.Error())
		}
	}()
	res, err := stmt.ExecContext(ctx,
		partnerChannel.PartnerID,
		partnerChannel.Name,
		partnerChannel.DisplayName,
		partnerChannel.Code,
		partnerChannel.SecretKey,
		partnerChannel.GrpCode,
		partnerChannel.Flag,
		partnerChannel.Type,
		partnerChannel.LogoUrl,
		partnerChannel.MinimumAmount,
		partnerChannel.IsManualPaymentAllowed,
		partnerChannel.Ordering,
		partnerChannel.IsActive,
		partnerChannel.NotifyUser,
		partnerChannel.DescriptionCode,
		partnerChannel.Timezone,
		partnerChannel.Version,
		partnerChannel.CreatedBy,
		partnerChannel.UpdatedBy,
		partnerChannel.DeletedBy,
	)
	if err != nil {
		return 0, ferror.Wrap(err, "exec stmt")
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, ferror.Wrap(err, "last insertID")
	}
	return int(id), nil
}

func (pc *PartnerChannel) Update(ctx context.Context, partnerChannelParam *domain.PartnerChannel) error {
	placeholder := sql.GetUpdateQSPlaceholderForColumns(partnerChannelColNamesForUpdate)
	query := fmt.Sprintf("UPDATE %v SET %v WHERE %v = %v AND version = %d", partnerChannelTableName, placeholder, partnerChannelColID, partnerChannelParam.ID, partnerChannelParam.Version)
	stmt, err := pc.db.PrepareContext(ctx, query)
	if err != nil {
		return ferror.Wrap(err, "prepare query")
	}

	defer func() {
		if err = stmt.Close(); err != nil {
			logger.Error(ctx, "Error in closing statement %v", err.Error())
		}
	}()

	res, err := stmt.ExecContext(
		ctx,
		partnerChannelParam.Name,
		partnerChannelParam.DisplayName,
		partnerChannelParam.Type,
		partnerChannelParam.LogoUrl,
		partnerChannelParam.IsActive,
		partnerChannelParam.NotifyUser,
		partnerChannelParam.Version,
		partnerChannelParam.UpdatedBy,
	)
	if err != nil {
		return ferror.Wrap(err, "execute stmt")
	}

	numberOfRows, err := res.RowsAffected()
	if err != nil {
		return ferror.Wrap(err, "rows affected result")
	}
	if numberOfRows == 0 {
		return ferror.Wrap(err, "Number of rows affected is zero")
	}

	return nil
}

// FindByID - Function that returns PartnerChannel from partner_channel table for given ID
func (pc *PartnerChannel) FindByID(ctx context.Context, id int) (*domain.PartnerChannel, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allPartnerChannelColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, partnerChannelTableName, partnerChannelColID, partnerChannelColDeletedAt)
	var partnerChannel *domain.PartnerChannel
	scan := func(row sql.Row) error {
		tPartnerChannel := new(domain.PartnerChannel)
		if err := row.Scan(
			&tPartnerChannel.ID,
			&tPartnerChannel.PartnerID,
			&tPartnerChannel.Name,
			&tPartnerChannel.DisplayName,
			&tPartnerChannel.Code,
			&tPartnerChannel.SecretKey,
			&tPartnerChannel.GrpCode,
			&tPartnerChannel.Flag,
			&tPartnerChannel.Type,
			&tPartnerChannel.LogoUrl,
			&tPartnerChannel.MinimumAmount,
			&tPartnerChannel.IsManualPaymentAllowed,
			&tPartnerChannel.Ordering,
			&tPartnerChannel.IsActive,
			&tPartnerChannel.NotifyUser,
			&tPartnerChannel.DescriptionCode,
			&tPartnerChannel.Timezone,
			&tPartnerChannel.Version,
			&tPartnerChannel.CreatedAt,
			&tPartnerChannel.CreatedBy,
			&tPartnerChannel.UpdatedAt,
			&tPartnerChannel.UpdatedBy,
			&tPartnerChannel.DeletedAt,
			&tPartnerChannel.DeletedBy,
		); err != nil {
			return ferror.Wrap(err, "row scan")
		}
		partnerChannel = tPartnerChannel
		return nil
	}
	if err := sql.Fetch(ctx, pc.db, scan, query, id); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}
	return partnerChannel, nil
}

func (pc *PartnerChannel) FindByEntityIDAndActiveAndMinAmountGTOrderBYOrdering(ctx context.Context, entityID int, amount int) ([]*domain.PartnerChannel, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allPartnerChannelColNames)
	query := fmt.Sprintf("SELECT %v FROM %v join %v on %v  WHERE %v = ? AND %v = ? AND %v = ? ORDER BY %v",
		columnNames, partnerChannelTableName, partnerTableName, partnerChannelJoinCols, partnerColEntityID, partnerChannelColMinimumAmount, partnerChannelColIsActive, partnerChannelColOrdering)

	var partnerChannels []*domain.PartnerChannel
	scan := func(row sql.Row) error {
		tPartnerChannel := new(domain.PartnerChannel)
		if err := row.Scan(
			&tPartnerChannel.ID,
			&tPartnerChannel.PartnerID,
			&tPartnerChannel.Name,
			&tPartnerChannel.DisplayName,
			&tPartnerChannel.Code,
			&tPartnerChannel.Type,
			&tPartnerChannel.LogoUrl,
			&tPartnerChannel.MinimumAmount,
			&tPartnerChannel.IsManualPaymentAllowed,
			&tPartnerChannel.Ordering,
			&tPartnerChannel.IsActive,
			&tPartnerChannel.NotifyUser,
			&tPartnerChannel.DescriptionCode,
			&tPartnerChannel.Timezone,
			&tPartnerChannel.Version,
			&tPartnerChannel.CreatedAt,
			&tPartnerChannel.CreatedBy,
			&tPartnerChannel.UpdatedAt,
			&tPartnerChannel.UpdatedBy,
			&tPartnerChannel.DeletedAt,
			&tPartnerChannel.DeletedBy,
		); err != nil {
			return ferror.Wrap(err, "row scan")
		}
		partnerChannels = append(partnerChannels, tPartnerChannel)
		return nil
	}
	if err := sql.Fetch(ctx, pc.db, scan, query, entityID, amount, true); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}
	return partnerChannels, nil
}

func (pc *PartnerChannel) FindByCode(ctx context.Context, code string) (*domain.PartnerChannel, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allPartnerChannelColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, partnerChannelTableName, partnerChannelColCode, partnerChannelColDeletedAt)

	var partnerChannel *domain.PartnerChannel
	scan := func(row sql.Row) error {
		tPartnerChannel := new(domain.PartnerChannel)
		if err := row.Scan(
			&tPartnerChannel.ID,
			&tPartnerChannel.PartnerID,
			&tPartnerChannel.Name,
			&tPartnerChannel.DisplayName,
			&tPartnerChannel.Code,
			&tPartnerChannel.Type,
			&tPartnerChannel.LogoUrl,
			&tPartnerChannel.MinimumAmount,
			&tPartnerChannel.IsManualPaymentAllowed,
			&tPartnerChannel.Ordering,
			&tPartnerChannel.IsActive,
			&tPartnerChannel.NotifyUser,
			&tPartnerChannel.DescriptionCode,
			&tPartnerChannel.Timezone,
			&tPartnerChannel.Version,
			&tPartnerChannel.CreatedAt,
			&tPartnerChannel.CreatedBy,
			&tPartnerChannel.UpdatedAt,
			&tPartnerChannel.UpdatedBy,
			&tPartnerChannel.DeletedAt,
			&tPartnerChannel.DeletedBy,
		); err != nil {
			return ferror.Wrap(err, "row scan")
		}
		partnerChannel = tPartnerChannel
		return nil
	}
	if err := sql.Fetch(ctx, pc.db, scan, query, code); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}
	return partnerChannel, nil
}

func (pc *PartnerChannel) FindByPartnerID(ctx context.Context, partnerID int) (*domain.PartnerChannel, error) {
	columNames := sql.GetSelectQSPlaceholderForColumns(allPartnerChannelColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v is NULL", columNames, partnerChannelTableName, partnerChannelColPartnerID, partnerChannelColDeletedAt)

	var partnerChannel *domain.PartnerChannel
	scan := func(row sql.Row) error {
		tPartnerChannel := new(domain.PartnerChannel)
		if err := row.Scan(
			&tPartnerChannel.ID,
			&tPartnerChannel.PartnerID,
			&tPartnerChannel.Name,
			&tPartnerChannel.DisplayName,
			&tPartnerChannel.Code,
			&tPartnerChannel.Type,
			&tPartnerChannel.LogoUrl,
			&tPartnerChannel.MinimumAmount,
			&tPartnerChannel.IsManualPaymentAllowed,
			&tPartnerChannel.Ordering,
			&tPartnerChannel.IsActive,
			&tPartnerChannel.NotifyUser,
			&tPartnerChannel.DescriptionCode,
			&tPartnerChannel.Timezone,
			&tPartnerChannel.Version,
			&tPartnerChannel.CreatedAt,
			&tPartnerChannel.CreatedBy,
			&tPartnerChannel.UpdatedAt,
			&tPartnerChannel.UpdatedBy,
			&tPartnerChannel.DeletedAt,
			&tPartnerChannel.DeletedBy,
		); err != nil {
			return ferror.Wrap(err, "row scan")
		}
		partnerChannel = tPartnerChannel
		return nil
	}
	if err := sql.Fetch(ctx, pc.db, scan, query, partnerID); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}

	return partnerChannel, nil
}
