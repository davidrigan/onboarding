// kafka config - One
package broker

import "bitbucket.org/finaccelteam/ms-common-go/broker/kafka"

var kafkaCfg kafkaConfig

type kafkaConfig struct {
	BootstrapServer        string     `mapstructure:"bootstrapServers"`
	SASLConfig             SASLConfig `mapstructure:"sasl_config"`
	DisableSSLVerification *bool      `mapstructure:"disableSSLVerification"`
	Awk                    string     `mapstructure:"ack"`
}

type SASLConfig struct {
	Username   string `mapstructure:"username"`
	Password   string `mapstructure:"password"`
	Protocol   string `mapstructure:"protocol"`
	Mechanisms string `mapstructure:"mechanisms"`
}

func (c *kafkaConfig) LoadInto() interface{} {
	return &c
}

func GetKafkaConfig() *kafkaConfig {
	return &kafkaCfg
}

//GetSASLKafkaConfig - return SASLKafkaConfig
func GetSASLKafkaConfig(config SASLConfig) kafka.SASLConfig {
	return kafka.SASLConfig{
		Username:   config.Username,
		Protocol:   config.Protocol,
		Mechanisms: config.Mechanisms,
		Password:   config.Password,
	}
}
