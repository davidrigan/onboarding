package handler

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/util/constants"
	"bitbucket.org/finaccelteam/onboarding/broker/publisher"
	incomingEvent "bitbucket.org/finaccelteam/onboarding/domain/incomingEvent"
	outgoingEvent "bitbucket.org/finaccelteam/onboarding/domain/outgoingEvent"
	"bitbucket.org/finaccelteam/onboarding/enum"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"context"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"os"
	"sync"
)

type partnerCreation struct {
	creationValidator  *validator.Validate
	creationSubscriber broker.Subscriber
	creationPublisher  broker.Publisher
	service            partner.Service
	isDispatch         bool
	repoOutgoingEvent  outgoingEvent.Repository
	repoIncomingEvent  incomingEvent.IncomingEventRepository
}

func (p *partnerCreation) StartSubscriber(wg *sync.WaitGroup, exit <-chan os.Signal, isDLQueue bool) error {
	ctx := context.Background()
	err := p.creationSubscriber.Subscribe(ctx, p.Execute) // change to Execute instead direct others methods
	if err != nil {
		return err
	}
	return nil
}

func (p *partnerCreation) Execute(ctx context.Context, m broker.PublishedMessage) error {
	// get EventID from messages
	incomingEventID := m.EventID()

	// set value of EventIDKey to context with incomingEventID
	ctx = context.WithValue(ctx, constants.EventIDKey, incomingEventID)

	// call the method to save data
	if err := p.PartnerCreationHandler(ctx, m); err != nil {
		return buildErrorEvent(err)
	}

	// generate new uuid for eventID
	newEventID := uuid.New().String()

	// get message and convert to string
	payload := string(m.Message())

	// set value to struct
	event := &outgoingEvent.OutgoingEvent{
		EventID:           newEventID,
		EventType:         enum.IncomingEventType.Partner,
		IncomingRequestID: incomingEventID,
		Payload:           payload,
		Version:           0,
	}

	// save incoming event
	if _, err := p.repoOutgoingEvent.Save(ctx, event); err != nil {
		return errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "Execute")
	}

	// calling func for publish the messages - need to injection broker publisher config
	if err := publisher.PublishOutgoingEvents(ctx, p.creationPublisher, p.repoOutgoingEvent, incomingEventID); err != nil {
		return buildErrorEvent(err)
	}

	// give logging if success
	logger.Info(ctx, "Successfully published Partner Created event for EventID %v", event.EventID)
	return nil
}

func (p *partnerCreation) PartnerCreationHandler(ctx context.Context, m broker.PublishedMessage) error {
	var err error
	requestBody := &request.Partner{}

	// i can use NewError for simulate if unmarshall process is failed, messages will throw to retry topic
	logger.Info(ctx, "Unmarshall data partner: %v", string(m.Message()))
	if err = json.Unmarshal(m.Message(), requestBody); err != nil {
		return errors.ErrKindUnprocessableWithCause(errors.ErrCodeInvalidUnmarshall, err, "PartnerCreationHandler")
	}

	// if failed to validate so we can throw to dlq topic
	logger.Info(ctx, "Validating data : %v", requestBody)
	if err = p.creationValidator.Struct(requestBody); err != nil {
		return errors.ErrKindUnprocessableWithCause(errors.ErrCodeInvalidValidateStruct, err, "PartnerCreationHandler")
	}

	// save data to db
	var partnerID int
	partnerID, err = p.service.CreatePartner(ctx, requestBody)
	if err != nil {
		return ferror.Wrap(err, "CreatePartner")
	}

	logger.Info(ctx, "Successfully consume message for partnerID: %v", partnerID)
	return nil
}

// NewPartnerCreation - Creates and returns a new insurance creation Subscriber
func NewPartnerCreation(kafkaSubscriber broker.Subscriber, creationPublisher broker.Publisher, service partner.Service, repoOutgoingEvent outgoingEvent.Repository, repoIncomingEvent incomingEvent.IncomingEventRepository) EventHandler {
	return &partnerCreation{
		creationValidator:  validator.New(),
		creationSubscriber: kafkaSubscriber,
		creationPublisher:  creationPublisher,
		service:            service,
		repoOutgoingEvent:  repoOutgoingEvent,
		repoIncomingEvent:  repoIncomingEvent,
	}
}
