package errors

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
)

const (
	// related Internal Server Error
	ErrCodeInternalServerError = ferror.Code("internal_server_error")
	ErrCodeRepositoryError     = ferror.Code("repository_error")

	// related business validation
	ErrCodePartnerExists        = ferror.Code("partner_exists")
	ErrCodePartnerChannelExists = ferror.Code("partner_channel_exists")
	ErrCodeIdempotencyMissing   = ferror.Code("idempotency_key_missing") // bad request
	ErrCodeBadMessages          = ferror.Code("bad_messages")
	ErrCodeBadRequest           = ferror.Code("bad_requests")

	// related not found
	ErrCodeResourcesMissing       = ferror.Code("resources_missing")
	ErrCodePartnerNotFound        = ferror.Code("partner_not_found")
	ErrCodePartnerChannelNotFound = ferror.Code("partner_channel_not_found")

	// related unprocessable
	ErrCodeInvalidDecode         = ferror.Code("invalid_decode")
	ErrCodeInvalidUnmarshall     = ferror.Code("invalid_unmarshall")
	ErrCodeInvalidMarshall       = ferror.Code("invalid_marshall")
	ErrCodeInvalidValidateStruct = ferror.Code("invalid_validate_struct")
)
