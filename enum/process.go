package enum

var Processes = struct {
	Partner        string
	PartnerChannel string
}{
	Partner:        "partner",
	PartnerChannel: "partner-channel",
}
