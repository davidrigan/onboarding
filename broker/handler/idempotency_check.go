package handler

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	domain "bitbucket.org/finaccelteam/onboarding/domain/incomingEvent"
	"bitbucket.org/finaccelteam/onboarding/handler"
	"context"
)

type IdempotencyMiddleWare struct {
	tx        sql.Transactional
	eventType int
	repo      domain.IncomingEventRepository
}

func (i *IdempotencyMiddleWare) Middleware() func(h broker.Handler) broker.Handler {
	return func(h broker.Handler) broker.Handler {
		return broker.HandlerFunc(func(ctx context.Context, m broker.PublishedMessage) error {
			// get EventID from messages
			incomingEventID := m.EventID()

			if err := i.tx.WithTransaction(ctx, func(ctx context.Context) error {

				// idempotency check, are eventId
				isProcessed, err := handler.IsEventProcessed(ctx, incomingEventID, i.eventType, i.repo)
				if err != nil {
					return ferror.Wrap(err, "Is Event Processed")
				}

				// if data is already processing
				if isProcessed {
					logger.Info(ctx, "Event is already processed for event ID %s", incomingEventID)
					return nil
				}

				// when we have bad messages, the event_id should not save to db
				// TODO invalid memory address here
				if err = h.Consume(ctx, m); err != nil {
					if err, ok := err.(*broker.BadMessageError); ok {
						return broker.NewBadMessageError(err.Code(), err, "[brokerapm.Middleware] error while consuming message")
					}
					return ferror.Wrap(err, "error while consuming message")
				}

				//save incoming event
				event := &domain.IncomingEvent{
					EventID:   incomingEventID,
					EventType: i.eventType,
					Version:   0,
				}

				_, err = i.repo.Save(ctx, event)
				if err != nil {
					return ferror.Wrap(err, "Save Incoming Event")
				}

				return nil
			}); err != nil {
				return ferror.Wrap(err, "With txn")
			}

			return nil
		})
	}
}

func NewIdempotencyMiddleWare(tx sql.Transactional, repo domain.IncomingEventRepository) (*IdempotencyMiddleWare, error) {
	return &IdempotencyMiddleWare{
		tx:   tx,
		repo: repo,
	}, nil
}
