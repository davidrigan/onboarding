// update this test for more flexible
// create request mocks for get data, place inside request/partner/mocks
// define request mocks in top of function / inside
// change test case prepareMock, assertCalled only accept t *testing, so will be more flexible when we use request for partner / update partner
// and then, when we assertCalled when more flexible to call the parameter
package partner

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	ferror2 "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
	domainPartner "bitbucket.org/finaccelteam/onboarding/domain/partner"
	MockPartnerRepo "bitbucket.org/finaccelteam/onboarding/domain/partner/mocks"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"bitbucket.org/finaccelteam/onboarding/request"
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"testing"
)

// yang menentukan isi dari testCase adalah parameter apa yang dibutuhkan dari fungsi yang akan di test
// TODO Notes -> we can group test cases positive and negative test cases
type partnerTestCase struct {
	name              string
	request           *request.Partner
	requestUpdate     *request.UpdatePartner
	wantErr           bool
	expectedErrorKind ferror2.Kind
	expectedErrorCode ferror.Code
	partnerID         int
	justPrepareMock   func(t *testing.T)
	justAssertCalled  func(t *testing.T)
}

// yang menentukan ketika membuat sebuah implementasi dari interfaces
// The suite package provides functionality that you might be used to from more common object oriented languages.
// With it, you can build a testing suite as a struct,
type partnerTestSuite struct {
	suite.Suite
	repoPartner MockPartnerRepo.Repository
	svcPartner  Service
	ctx         context.Context
}

// mengisi nilai dari implementasi yang di butuhkan
// build setup/teardown methods and testing methods on your struct, and run them with 'go test' as per normal.
func (s *partnerTestSuite) Setup() {
	s.repoPartner = MockPartnerRepo.Repository{}
	s.svcPartner = NewService(&s.repoPartner)
	s.ctx = context.Background()
}

func (s *partnerTestSuite) TestGetPartners() {
	testCases := []partnerTestCase{
		{
			name:    "partner repo Get, found data",
			wantErr: false,
			justPrepareMock: func(t *testing.T) {
				partner := []*domainPartner.Partner{
					{
						ID:          1,
						Name:        "FasaPay",
						MerchantID:  "fasa",
						GatewayType: 11,
						IsActive:    true,
						EntityID:    9,
					},
					{
						ID:          2,
						Name:        "Midtrans",
						MerchantID:  "mid",
						GatewayType: 15,
						IsActive:    false,
						EntityID:    10,
					},
				}
				s.repoPartner.On("Get", mock.Anything).Return(partner, nil)
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "Get", mock.Anything)
			},
		},
		{
			name:              "partner repo get, error repo",
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("Get", mock.Anything).Return(nil, ferror.E("Some errors"))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "Get", mock.Anything)
			},
		},
	}

	for _, testCase := range testCases {
		s.T().Run(testCase.name, func(t *testing.T) {
			s.Setup()
			testCase.justPrepareMock(t)
			_, err := s.svcPartner.GetPartners(s.ctx)
			if testCase.wantErr {
				assert.True(t, ferror.Is(err, testCase.expectedErrorCode), "error expected: %v, but got: %v", testCase.expectedErrorCode, err)
				assert.True(t, ferror2.IsOfKind(err, testCase.expectedErrorKind), "error expected: %v, but got: %v", testCase.expectedErrorKind, err)
			}
			testCase.justAssertCalled(t)
		})
	}
}

func (s *partnerTestSuite) TestGetPartner() {
	testCases := []partnerTestCase{
		{
			name:              "partner test repo FindByID, partner is not exists",
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(nil, ferror.E("Data not exists"))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
			},
		},
		{
			name:              "partner test repo FindByID, partner is empty",
			wantErr:           true,
			expectedErrorKind: errors.ErrNotFoundKind,
			expectedErrorCode: errors.ErrCodePartnerNotFound,
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(nil, nil)
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
			},
		},
	}

	for _, testCase := range testCases {
		s.T().Run(testCase.name, func(t *testing.T) {
			s.Setup()
			testCase.justPrepareMock(t)
			_, err := s.svcPartner.GetPartner(s.ctx, 1)
			if testCase.wantErr {
				assert.True(t, ferror.Is(err, testCase.expectedErrorCode), "error expected: %v, but got: %v", testCase.expectedErrorCode, err)
				assert.True(t, ferror2.IsOfKind(err, testCase.expectedErrorKind), "error expected: %v, but got: %v", testCase.expectedErrorKind, err)
			}
			testCase.justAssertCalled(t)
		})
	}
}

func (s *partnerTestSuite) TestCreatePartner() {
	testCases := []partnerTestCase{
		{
			name:              "partner request is empty",
			wantErr:           true,
			expectedErrorCode: errors.ErrCodeBadRequest,
			expectedErrorKind: errors.ErrUnprocessableKind,
		},
		{
			name:              "partner Save repo, save error",
			wantErr:           true,
			request:           s.reqPartner("FasaPay", "fasa", 11, true, 9),
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("Save", mock.Anything, mock.Anything).Return(-1, ferror.E("some error")).Run(s.validateCreatePartner(t, s.mockPartner()))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "Save", mock.Anything, mock.Anything)
			},
		},
		{
			name:    "partner Save repo",
			wantErr: false,
			request: s.reqPartner("FasaPay", "fasa", 11, true, 9),
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("Save", mock.Anything, mock.Anything).Return(1, nil).Run(s.validateCreatePartner(t, s.mockPartner()))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "Save", mock.Anything, mock.Anything)
			},
		},
	}

	for _, testCase := range testCases {
		s.T().Run(testCase.name, func(t *testing.T) {
			s.Setup()

			// we should check are the request is nill or not when create / update data
			if testCase.request != nil {
				testCase.justPrepareMock(t)
			}

			_, err := s.svcPartner.CreatePartner(s.ctx, testCase.request)

			if testCase.wantErr {
				assert.True(t, ferror.Is(err, testCase.expectedErrorCode), "error expected: %v, but got: %v", testCase.expectedErrorCode, err)
				assert.True(t, ferror2.IsOfKind(err, testCase.expectedErrorKind), "error expected: %v, but got: %v", testCase.expectedErrorKind, err)
			}

			if testCase.request != nil {
				testCase.justAssertCalled(t)
			}

		})
	}
}

func (s *partnerTestSuite) TestUpdatePartner() {
	testCases := []partnerTestCase{
		{
			name:              "partner request is empty",
			wantErr:           true,
			expectedErrorCode: errors.ErrCodeBadRequest,
			expectedErrorKind: errors.ErrUnprocessableKind,
		},
		{
			name:              "partner FindByID repo, error",
			requestUpdate:     s.reqUpdatePartner("Fasa Edit", "fasa edit", 9, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			partnerID:         1,
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(nil, ferror.E("some error"))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
			},
		},
		{
			name:              "partner FindByID repo, data not exists",
			requestUpdate:     s.reqUpdatePartner("Fasa Edit", "fasa edit", 9, true),
			wantErr:           true,
			expectedErrorKind: errors.ErrNotFoundKind,
			expectedErrorCode: errors.ErrCodePartnerNotFound,
			partnerID:         1,
			justPrepareMock: func(t *testing.T) {
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(nil, nil)
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
			},
		},
		{
			name:          "partner Update repo",
			requestUpdate: s.reqUpdatePartner("Fasa Edit", "fasa edit", 9, true),
			wantErr:       false,
			partnerID:     1,
			justPrepareMock: func(t *testing.T) {
				requestCheck := &domainPartner.Partner{Name: "Fasapay Edit", MerchantID: "fasa edit", GatewayType: 9, IsActive: true}
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(requestCheck, nil)
				s.repoPartner.On("Update", mock.Anything, mock.Anything).Return(nil).Run(s.validateCreatePartner(t, s.mockUpdatePartner()))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
				s.repoPartner.AssertCalled(t, "Update", mock.Anything, mock.Anything)
			},
		},
		{
			name:              "partner Update repo, error when update",
			requestUpdate:     s.reqUpdatePartner("Fasa Edit", "fasa edit", 9, true),
			wantErr:           true,
			partnerID:         1,
			expectedErrorCode: errors.ErrCodeRepositoryError,
			expectedErrorKind: errors.ErrInternalServerErrorKind,
			justPrepareMock: func(t *testing.T) {
				requestCheck := &domainPartner.Partner{Name: "Fasapay Edit", MerchantID: "fasa edit", GatewayType: 9, IsActive: true}
				s.repoPartner.On("FindByID", mock.Anything, mock.Anything).Return(requestCheck, nil)
				s.repoPartner.On("Update", mock.Anything, mock.Anything).Return(ferror.E("some error")).Run(s.validateCreatePartner(t, s.mockUpdatePartner()))
			},
			justAssertCalled: func(t *testing.T) {
				s.repoPartner.AssertCalled(t, "FindByID", mock.Anything, mock.Anything)
				s.repoPartner.AssertCalled(t, "Update", mock.Anything, mock.Anything)
			},
		},
	}
	for _, testCase := range testCases {
		s.T().Run(testCase.name, func(t *testing.T) {
			s.Setup()

			if testCase.requestUpdate != nil {
				testCase.justPrepareMock(t)
			}

			err := s.svcPartner.UpdatePartner(s.ctx, testCase.requestUpdate, testCase.partnerID)

			if testCase.wantErr {
				assert.True(t, ferror.Is(err, testCase.expectedErrorCode), "error expected: %v, but got: %v", testCase.expectedErrorCode, err)
				assert.True(t, ferror2.IsOfKind(err, testCase.expectedErrorKind), "error expected: %v, but got: %v", testCase.expectedErrorKind, err)
			}

			if testCase.requestUpdate != nil {
				testCase.justAssertCalled(t)
			}

		})
	}
}

// we must validate the value when create or update
// validate method we can call on .Run before process data
func (s *partnerTestSuite) validateCreatePartner(t *testing.T, expected *domainPartner.Partner) func(args mock.Arguments) {
	return func(args mock.Arguments) {
		actualPartner := args[1].(*domainPartner.Partner)
		assert.Equal(t, expected.Name, actualPartner.Name, "expected %v found %v", expected.Name, actualPartner.Name)
		assert.Equal(t, expected.MerchantID, actualPartner.MerchantID, "expected %v found %v", expected.MerchantID, actualPartner.MerchantID)
		assert.Equal(t, expected.GatewayType, actualPartner.GatewayType, "expected %v found %v", expected.GatewayType, actualPartner.GatewayType)
		assert.Equal(t, expected.IsActive, actualPartner.IsActive, "expected %v found %v", expected.IsActive, actualPartner.IsActive)
		assert.Equal(t, expected.EntityID, actualPartner.EntityID, "expected %v found %v", expected.EntityID, actualPartner.EntityID)
		expected.ID = actualPartner.ID
		expected.Name = actualPartner.Name
		expected.MerchantID = actualPartner.MerchantID
		expected.GatewayType = actualPartner.GatewayType
		expected.IsActive = actualPartner.IsActive
		expected.EntityID = actualPartner.EntityID
		expected.CreatedBy = actualPartner.CreatedBy
		expected.UpdatedBy = actualPartner.UpdatedBy
		assert.EqualValues(t, expected, actualPartner)
	}
}

func (s *partnerTestSuite) reqPartner(name string, merchantID string, gatewayType int, isActive bool, entityID int) *request.Partner {
	return &request.Partner{
		Name:        name,
		MerchantID:  merchantID,
		GatewayType: gatewayType,
		IsActive:    &isActive,
		EntityID:    entityID,
	}
}

func (s *partnerTestSuite) reqUpdatePartner(name string, merchantID string, gatewayType int, isActive bool) *request.UpdatePartner {
	return &request.UpdatePartner{
		Name:        &name,
		MerchantID:  &merchantID,
		GatewayType: &gatewayType,
		IsActive:    &isActive,
	}
}

// mock domain values of partner
func (s partnerTestSuite) mockUpdatePartner() *domainPartner.Partner {
	return &domainPartner.Partner{
		ID:          1,
		Name:        "Fasa Edit",
		MerchantID:  "fasa edit",
		GatewayType: 9,
		IsActive:    true,
		//EntityID:    1,
	}
}

// mock domain values of partner
func (s partnerTestSuite) mockPartner() *domainPartner.Partner {
	return &domainPartner.Partner{
		ID:          1,
		Name:        "FasaPay",
		MerchantID:  "fasa",
		GatewayType: 11,
		IsActive:    true,
		EntityID:    9,
	}
}

// mock domain values of partners
func (s *partnerTestSuite) mockAllPartner() []*domainPartner.Partner {
	return []*domainPartner.Partner{
		{
			ID:          1,
			Name:        "FasaPay",
			MerchantID:  "fasa",
			GatewayType: 11,
			IsActive:    true,
			EntityID:    9,
		},
		{
			ID:          2,
			Name:        "Midtrans",
			MerchantID:  "mid",
			GatewayType: 15,
			IsActive:    false,
			EntityID:    10,
		},
	}
}

// run all test, attach to partnerTestSuite
func TestPartnerTestSuite(t *testing.T) {
	suite.Run(t, new(partnerTestSuite))
}
