package container

var (
	mySQLDBConfig MySQLDBConfig
	serverCfg     Config
)

//DBConfig interface for database config
type DBConfig interface {
	GetHost() string
	GetPort() string
	GetUser() string
	GetPassword() string
	GetDatabase() string
	GetDriver() string
	GetMaxOpenConnections() int
	GetMaxIdleConnections() int
	LoadInto() interface{}
}

//MySQLDBConfig - config struct for database
type MySQLDBConfig struct {
	Host               string `mapstructure:"host"`
	Port               string `mapstructure:"port"`
	User               string `mapstructure:"user"`
	Password           string `mapstructure:"password"`
	Database           string `mapstructure:"db"`
	Driver             string `mapstructure:"driver"`
	MaxOpenConnections int    `mapstructure:"maxOpenConnections"`
	MaxIdleConnections int    `mapstructure:"maxIdleConnections"`
}

//GetHost - Returns the host
func (c *MySQLDBConfig) GetHost() string {
	return c.Host
}

//GetPort - Returns the port
func (c *MySQLDBConfig) GetPort() string {
	return c.Port
}

//GetUser - Returns the user
func (c *MySQLDBConfig) GetUser() string {
	return c.User
}

//GetPassword - Returns the password
func (c *MySQLDBConfig) GetPassword() string {
	return c.Password
}

//GetDatabase - Returns the database
func (c *MySQLDBConfig) GetDatabase() string {
	return c.Database
}

//GetDriver - Returns the database driver
func (c *MySQLDBConfig) GetDriver() string {
	return c.Driver
}

//GetMaxOpenConnections - Returns the maximum open connections
func (c *MySQLDBConfig) GetMaxOpenConnections() int {
	return c.MaxOpenConnections
}

//GetMaxIdleConnections - Returns the maximum idle connections
func (c *MySQLDBConfig) GetMaxIdleConnections() int {
	return c.MaxIdleConnections
}

//LoadInto - returns the pointer to the struct
func (c *MySQLDBConfig) LoadInto() interface{} {
	return &c
}

//GetMySQLDBConfig - return the pointer to the var dbConfig
func GetMySQLDBConfig() DBConfig {
	return &mySQLDBConfig
}

//Config - Struct containing the config data
type Config struct {
	Name    string `mapstructure:"name"`
	Port    int    `mapstructure:"port"`
	Version string `mapstructure:"version"`
}

//LoadInto - returns the struct c to load the config data into
func (s *Config) LoadInto() interface{} {
	return &s
}

//GetName - returns the name of the server
func (s *Config) GetName() string {
	return s.Name
}

//GetVersion - returns the Version of the server
func (s *Config) GetVersion() string {
	return s.Version
}

//GetPort - returns the port the server should run on
func (s *Config) GetPort() int {
	return s.Port
}

//GetServerConfig - returns the ServerConfig
func GetServerConfig() *Config {
	return &serverCfg
}
