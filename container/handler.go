package container

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	idempotency "bitbucket.org/finaccelteam/onboarding/broker/handler"
	"bitbucket.org/finaccelteam/onboarding/broker/publisher"
	"bitbucket.org/finaccelteam/onboarding/broker/subscriber"
	"bitbucket.org/finaccelteam/onboarding/handler"
	"bitbucket.org/finaccelteam/onboarding/repository"
	"context"
)

var (
	partnerCreationHandler        handler.EventHandler
	partnerChannelCreationHandler handler.EventHandler
	idempotencyHandler            *idempotency.IdempotencyMiddleWare
)

func PartnerHandler(ctx context.Context, isDlq bool, numberOfConsumers int) (handler.EventHandler, error) {
	if partnerCreationHandler == nil {
		kafkaSubscriberOpt := subscriber.GetPartnerCreateKafkaOption()
		kafkaSubscriber, err := subscriber.GetKafkaEventSubscriber(handler.GetCreatePartnerKafkaConfig(), isDlq, numberOfConsumers, kafkaSubscriberOpt)
		if err != nil {
			return nil, ferror.Wrap(err, "Kafka subscriber error")
		}

		idempotency, err := IdempotencyHandler(ctx)
		if err != nil {
			return nil, ferror.Wrap(err, "Idempotency")
		}
		kafkaSubscriber.Use(idempotency.Middleware())

		kafkaPublisher, err := publisher.GetKafkaEventPublisher()
		if err != nil {
			return nil, ferror.Wrap(err, "Kafka publisher error")
		}

		servicePartner, err := getPartnerService(ctx)
		if err != nil {
			return nil, ferror.Wrap(err, "Service Partner")
		}

		repoOutgoingEvent, err := getOutGoingEventRepository(ctx)
		if err != nil {
			return nil, ferror.Wrap(err, "Repo Outgoing Event")
		}

		repoIncomingEvent, err := getIncomingEventRepository(ctx)
		if err != nil {
			return nil, ferror.Wrap(err, "Repo Incoming Event")
		}

		partnerCreationHandler = handler.NewPartnerCreation(
			kafkaSubscriber,
			kafkaPublisher,
			servicePartner,
			repoOutgoingEvent,
			repoIncomingEvent,
		)

		handler.RegisterEventHandler(partnerCreationHandler)
	}
	return partnerCreationHandler, nil
}

func PartnerChannelHandler(ctx context.Context, isDlq bool, numberOfConsumers int) (handler.EventHandler, error) {
	if partnerChannelCreationHandler == nil {
		kafkaSubscriberOpt := subscriber.GetPartnerCreateKafkaOption()
		kafkaSubscriber, err := subscriber.GetKafkaEventSubscriber(handler.GetCreatePartnerChannelKafkaConfig(), isDlq, numberOfConsumers, kafkaSubscriberOpt)
		if err != nil {
			return nil, ferror.Wrap(err, "Kafka subscriber error")
		}
		idempotency, err := IdempotencyHandler(ctx)
		if err != nil {
			return nil, ferror.Wrap(err, "Idempotency")
		}
		kafkaSubscriber.Use(idempotency.Middleware())

		servicePartner, err := getPartnerService(ctx)
		if err != nil {
			return nil, ferror.Wrap(err, "Service Partner")
		}
		servicePartnerChannel, err := getPartnerChannelService(ctx)
		if err != nil {
			return nil, ferror.Wrap(err, "Service Partner Channel")
		}

		partnerChannelCreationHandler = handler.NewPartnerChannelHandler(kafkaSubscriber, servicePartner, servicePartnerChannel)

		handler.RegisterEventHandler(partnerChannelCreationHandler)
	}
	return partnerChannelCreationHandler, nil
}

func IdempotencyHandler(ctx context.Context) (*idempotency.IdempotencyMiddleWare, error) {
	dbtx, err := getDBTX(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "DB error")
	}
	tx, err := GetTransactional(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "Transaction DB error")
	}
	repoIncomingEvent := repository.NewIncomingEventRepository(dbtx)
	idempotencyHandler, err = idempotency.NewIdempotencyMiddleWare(tx, repoIncomingEvent)
	if err != nil {
		return nil, ferror.Wrap(err, "Idempotency Error")
	}
	return idempotencyHandler, nil
}
