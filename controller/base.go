package controller

import (
	ferror2 "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"net/http"
)

func buildErrorResponse(err error, locale string) error {
	if ferr, ok := err.(ferror2.FError); ok {
		switch ferr.Kind() {
		case errors.ErrBusinessValidationKind:
			return fhttp.NewResponseErrorWithCause(ferr.Code(), err, "Business Validation", http.StatusBadRequest)
		case errors.ErrNotFoundKind:
			return fhttp.NewResponseErrorWithCause(ferr.Code(), err, "Not Found", http.StatusNotFound)
		case errors.ErrUnprocessableKind:
			return fhttp.NewResponseErrorWithCause(ferr.Code(), err, "Unprocessable", http.StatusUnprocessableEntity)
		case errors.ErrInternalServerErrorKind:
			return fhttp.NewResponseErrorWithCause(ferr.Code(), err, "Internal Server Error", http.StatusInternalServerError)
		default:
			return fhttp.NewResponseErrorWithCause(ferr.Code(), err, "No match any error kind", http.StatusInternalServerError)
		}
	} else {
		return fhttp.NewResponseErrorWithCause(errors.ErrCodeInternalServerError, err, "Internal Server Error", http.StatusInternalServerError)
	}
}
