//go:build wireinject
// +build wireinject

package service

import (
	"bitbucket.org/finaccelteam/onboarding/container"
	"bitbucket.org/finaccelteam/onboarding/repository"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"bitbucket.org/finaccelteam/onboarding/service/partnerChannel"
	"context"
	"github.com/google/wire"
)

var newServRepo = wire.NewSet(repository.NewPartnerRepository, repository.NewPartnerChannelRepository)

func getPartnerChannelService(ctx context.Context) (partnerChannel.Service, error) {
	wire.Build(container.DatabaseSet, newServRepo, partnerChannel.NewService)
	return nil, nil
}

func getPartnerService(ctx context.Context) (partner.Service, error) {
	wire.Build(container.DatabaseSet, repository.NewPartnerRepository, partner.NewService)
	return nil, nil
}
