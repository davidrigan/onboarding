package controller

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	commonController "bitbucket.org/finaccelteam/ms-common-go/controller"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"bitbucket.org/finaccelteam/ms-common-go/util/constants"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"bitbucket.org/finaccelteam/onboarding/middleware"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"context"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

// 4
type partnerController struct {
	service     partner.Service
	tx          sql.Transactional
	publish     broker.Publisher
	idempotency middleware.IdempotencyMiddleWare
	validate    *validator.Validate
}

func (p *partnerController) RegisterRoutes(router *mux.Router) {
	router.Handle("/partners", fhttp.AppHandler(p.getAll)).Methods("GET")
	router.Handle("/partners/{id:[0-9]+}", fhttp.AppHandler(p.getPartner)).Methods("GET")
	router.Handle("/partners", fhttp.AppHandler(p.createPartner)).Methods("POST")
	router.Handle("/partners/{id:[0-9]+}", fhttp.AppHandler(p.updatePartner)).Methods("PUT")
}

func (p *partnerController) getAll(r *http.Request) (*fhttp.Response, error) {
	var err error
	ctx := r.Context()
	// catch every query
	// instead using Queries methods in route handle, we can use manually get query using this method
	// because if we're using Queries method in route handle, will only match if the URL contains the defined queries values, e.g.: ?foo=bar&id=42.
	query := r.URL.Query()

	// check what queries are sent in request
	// improving to check value if send in queries parameter
	merchantID := query.Get("merchantID")
	gatewayTypeStr := query.Get("gatewayType")

	// convert gateway type from string to int
	var gatewayType int
	if gatewayTypeStr != "" {
		gatewayTypeParsing, err := strconv.Atoi(gatewayTypeStr)
		if err != nil {
			return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeResourcesMissing, err, "getPartner", http.StatusUnprocessableEntity)
		}
		gatewayType = gatewayTypeParsing
	}

	// transaction
	var resp []response.Partner
	if err = p.tx.WithTransaction(ctx, func(ctx context.Context) error {
		resp, err = p.service.FilterPartner(ctx, merchantID, gatewayType)
		if err != nil {
			return ferror.Wrap(err, "FilterPartner")
		}
		return nil
	}); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return &fhttp.Response{
		Data:       resp,
		StatusCode: http.StatusOK,
	}, nil
}

func (p *partnerController) getPartner(r *http.Request) (*fhttp.Response, error) {
	ctx := r.Context()
	partnerID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeResourcesMissing, err, "getPartner", http.StatusUnprocessableEntity)
	}

	res := &fhttp.Response{}

	var resp *response.Partner
	if err = p.idempotency.Idempotency(ctx, r, res, func(ctx context.Context, res *fhttp.Response) error {
		resp, err = p.service.GetPartner(ctx, partnerID)
		if err != nil {
			return ferror.Wrap(err, "GetPartner")
		}
		res.Data = resp
		res.StatusCode = http.StatusOK
		return nil
	}); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return res, nil
}

func (p *partnerController) createPartner(r *http.Request) (*fhttp.Response, error) {
	var err error
	ctx := r.Context()
	body := r.Body
	requestBody := &request.Partner{}
	res := &fhttp.Response{}

	if err = json.NewDecoder(body).Decode(requestBody); err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeInvalidDecode, err, "Business Validation", http.StatusUnprocessableEntity)
	}

	if err = p.validate.Struct(requestBody); err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeInvalidValidateStruct, err, "Business Validation", http.StatusBadRequest)
	}

	var partnerId int
	if err = p.idempotency.Idempotency(ctx, r, res, func(ctx context.Context, res *fhttp.Response) error {
		partnerId, err = p.service.CreatePartner(ctx, requestBody)
		if err != nil {
			return ferror.Wrap(err, "CreatePartner")
		}
		res.Data = partnerId
		res.StatusCode = http.StatusCreated
		return nil
	}); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return res, nil
}

func (p *partnerController) updatePartner(r *http.Request) (*fhttp.Response, error) {
	ctx := r.Context()
	partnerID, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeResourcesMissing, err, "updatePartner", http.StatusUnprocessableEntity)
	}
	req := &request.UpdatePartner{}
	res := &fhttp.Response{}

	logger.Info(ctx, "Decode data: %v", r.Body)
	if err = json.NewDecoder(r.Body).Decode(req); err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeInvalidDecode, err, "updatePartner", http.StatusUnprocessableEntity)
	}

	logger.Info(ctx, "Validating data: %v", req)
	if err = p.validate.Struct(req); err != nil {
		return nil, fhttp.NewResponseErrorWithCause(errors.ErrCodeInvalidValidateStruct, err, "updatePartner", http.StatusBadRequest)
	}

	if err = p.idempotency.Idempotency(ctx, r, res, func(ctx context.Context, res *fhttp.Response) error {
		err = p.service.UpdatePartner(ctx, req, partnerID)
		if err != nil {
			return ferror.Wrap(err, "UpdatePartner")
		}
		res.Data = req
		res.StatusCode = http.StatusOK
		return nil
	}); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	logger.Info(ctx, "Successfully update data for partnerID: %v", partnerID)
	return res, nil
}

// TODO publishPartner - improve how to publish message to broker
func (p *partnerController) publishPartner(r *http.Request) error {
	var err error
	ctx := r.Context()
	requestBody := &request.Partner{}

	if err = json.NewDecoder(r.Body).Decode(requestBody); err != nil {
		return fhttp.NewResponseErrorWithCause(errors.ErrCodeInvalidDecode, err, "publishPartner", http.StatusUnprocessableEntity)
	}
	if err = p.validate.Struct(requestBody); err != nil {
		return fhttp.NewResponseErrorWithCause(errors.ErrCodeInvalidValidateStruct, err, "publishPartner", http.StatusBadRequest)
	}

	eventID := uuid.New().String()
	newCtx := context.Background() // why we need new context ?

	if correlationID := ctx.Value(constants.CorrelationIDKey); correlationID != nil {
		newCtx = context.WithValue(newCtx, constants.CorrelationIDKey, correlationID)
	}

	// create service that receive parameter
	if err = p.service.PublishCreatePartner(newCtx, eventID, requestBody, p.publish); err != nil {
		return buildErrorResponse(err, "")
	}

	return nil
}

// NewPartnerController - Creates and returns a new NewPartnerController
func NewPartnerController(router *mux.Router, service partner.Service, tx sql.Transactional, publish broker.Publisher, idempotency middleware.IdempotencyMiddleWare) commonController.Controller {
	partnerController := &partnerController{
		service:     service,
		tx:          tx,
		publish:     publish,
		idempotency: idempotency,
		validate:    validator.New(),
	}
	partnerController.RegisterRoutes(router)
	return partnerController
}
