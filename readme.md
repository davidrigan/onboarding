### Dependency Injection
In software engineering, Dependency Injection is a technique in which one object supplies the dependencies of another object. 'Dependency' is an object that can be used, for example as a service. In practice, dependency injection is closely related to the SOLID principle.
In this project we can see implementation of this technique inside container package where controller depend on service and service depend on repository, but they are independent of its dependencies.

### How to we check nil value in struct request
Every struct request have validation like value is required to prevent client send empty data but when we have condition for updates, not all value is required have value and not all value is send. When check value is empty or not, we can using simple condition checking like this :
```go
if request.Name != "" {
	// do anything
}
```
But we still need send this value in body request. To solve this problem we can use pointer in data type of struct and remove validate in properties of variable, so we can check nil value or not
```go
....
	Name *string `json:"name"`
....
```

### SASL
So we use SASL for secure connections like HTTPS is essential between client applications and Kafka Server and many methods for authentication when we use SASL e.g SASL: PLAIN, SCRAM(SHA-256 and SHA-512), OAUTHBEARER, GSSAPI(Kerberos)
When we use SASL, we must change the port of kafka server based on listener protocol map, in this code we can refer to docker-compose file with key and value 
```dockerfile
KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: INTERNAL:PLAINTEXT,EXTERNAL:PLAINTEXT,EXTERNAL_SSL:SASL_SSL
KAFKA_ADVERTISED_LISTENERS: INTERNAL://kafka-1:19092,EXTERNAL://127.0.0.1:9092,EXTERNAL_SSL://127.0.0.1:9093
```
more information : https://medium.com/egen/securing-kafka-cluster-using-sasl-acl-and-ssl-dec15b439f9d


### Analyze Error

Just ferror.Wrap
```bash
{"Correlation-ID":"partner","Event-ID":"11bdb206-900e-49c6-8957-570fb6859fa8","File":"/Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/kafka/kafka_subscriber.go","Function":"bitbucket.org/finaccelteam/ms-common-go/broker/kafka.(*ConsumerWorker).handleMessage","Level":"ERROR","Line":"179","Message":["processing error: Code: {invalid_unmarshall}, Ctx: {With txn}, Cause: {Ctx: {fn execution}, Cause: {Ctx: {[brokerapm.Middleware] error while consuming message}, Cause: {Ctx: {PartnerCreationHandler}, Cause: {Kind: {UNPROCESSABLE}, Code: {invalid_unmarshall}, Code: {invalid_unmarshall}, Msg: {PartnerCreationHandler}, Cause: {json: cannot unmarshal string into Go struct field Partner.entity_id of type int}}}}}","(v2.NewWithCause) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/ferror/v2@v2.0.2-temp-2/errors.go:125","(errors.ErrKindUnprocessableWithCause) -- /Users/david/Documents/Learning/golang/onboarding/errors/errors.go:101","(handler.(*partnerCreation).PartnerCreationHandler) -- /Users/david/Documents/Learning/golang/onboarding/handler/partner.go:103","(handler.(*partnerCreation).Execute) -- /Users/david/Documents/Learning/golang/onboarding/handler/partner.go:62","(broker.HandlerFunc.Consume) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/subscriber.go:21","(handler.(*IdempotencyMiddleWare).Middleware.func1.1.1) -- /Users/david/Documents/Learning/golang/onboarding/broker/handler/idempotency_check.go:39","(sql.(*transactional).WithTransaction) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/sql@v1.2.3/transaction.go:65","(handler.(*IdempotencyMiddleWare).Middleware.func1.1) -- /Users/david/Documents/Learning/golang/onboarding/broker/handler/idempotency_check.go:25","(broker.HandlerFunc.Consume) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/subscriber.go:21","(kafka.(*ConsumerWorker).handleMessage) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/kafka/kafka_subscriber.go:172","(kafka.(*ConsumerWorker).run) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/kafka/kafka_subscriber.go:135","(kafka.(*Subscriber).Subscribe.func1) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/kafka/kafka_subscriber.go:76","(runtime.goexit) -- /usr/local/go/src/runtime/asm_amd64.s:1581"],"Service":"onboarding[Consumer]","Time":"2022-03-07 14:34:01.012607000"}
```

With buildErrorEvent
```bash
{"Correlation-ID":"partner","Event-ID":"3588cb91-0dad-45c3-b9ed-b03d91ba7497","File":"/Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/kafka/kafka_subscriber.go","Function":"bitbucket.org/finaccelteam/ms-common-go/broker/kafka.(*ConsumerWorker).handleMessage","Level":"ERROR","Line":"179","Message":["processing error: Code: {invalid_unmarshall}, Ctx: {With txn}, Cause: {Ctx: {fn execution}, Cause: {BAD_MESSAGE =\u003e Code: {invalid_unmarshall}, Msg: {[brokerapm.Middleware] error while consuming message}, Cause: {BAD_MESSAGE =\u003e Code: {invalid_unmarshall}, Msg: {Unprocessable}, Cause: {Kind: {UNPROCESSABLE}, Code: {invalid_unmarshall}, Code: {invalid_unmarshall}, Msg: {PartnerCreationHandler}, Cause: {json: cannot unmarshal string into Go struct field Partner.entity_id of type int}}}}}","(v2.NewWithCause) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/ferror/v2@v2.0.2-temp-2/errors.go:125","(errors.ErrKindUnprocessableWithCause) -- /Users/david/Documents/Learning/golang/onboarding/errors/errors.go:101","(handler.(*partnerCreation).PartnerCreationHandler) -- /Users/david/Documents/Learning/golang/onboarding/handler/partner.go:103","(handler.(*partnerCreation).Execute) -- /Users/david/Documents/Learning/golang/onboarding/handler/partner.go:62","(broker.HandlerFunc.Consume) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/subscriber.go:21","(handler.(*IdempotencyMiddleWare).Middleware.func1.1.1) -- /Users/david/Documents/Learning/golang/onboarding/broker/handler/idempotency_check.go:39","(sql.(*transactional).WithTransaction) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/sql@v1.2.3/transaction.go:65","(handler.(*IdempotencyMiddleWare).Middleware.func1.1) -- /Users/david/Documents/Learning/golang/onboarding/broker/handler/idempotency_check.go:25","(broker.HandlerFunc.Consume) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/subscriber.go:21","(kafka.(*ConsumerWorker).handleMessage) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/kafka/kafka_subscriber.go:172","(kafka.(*ConsumerWorker).run) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/kafka/kafka_subscriber.go:135","(kafka.(*Subscriber).Subscribe.func1) -- /Users/david/go/pkg/mod/bitbucket.org/finaccelteam/ms-common-go/broker@v1.4.5/kafka/kafka_subscriber.go:76","(runtime.goexit) -- /usr/local/go/src/runtime/asm_amd64.s:1581"],"Service":"onboarding[Consumer]","Time":"2022-03-07 14:40:45.022725000"}
```


### TODO
- Create consumer from partner channel who produce from kafka basic and then save the data to database but check first if data is exists or not and then implementation idempotency to
- Improve how we handle dependency injection in container
- 

### Asking
- Are we can implement for generate topic name automatically? it's allowed? not allowed in stag and prod
- i have see between domain struct data type different with database data type?
- why request struct using pointer ? because we can validate if value nil or not
- why we are checking value == nil in repo, service, handler?
- so i not seen idempotency from http request, because the ms lender load communication only via async?
- just confirmation again service of domain, because three of us still asking when we can implementation. my understanding is for create data for the first time and checking the data
- in database indexing video we must aware of several things like coloumn ordering, how to setting read indexing. If we in project we should focus on just query