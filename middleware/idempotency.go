package middleware

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"bitbucket.org/finaccelteam/ms-common-go/util/constants"
	domain "bitbucket.org/finaccelteam/onboarding/domain/idempotency"
	"bitbucket.org/finaccelteam/onboarding/errors"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

const (
	IdempotencyKeyLockTimeout = 300 // 5 minutes
)

type IdempotencyMiddleWare interface {
	Idempotency(ctx context.Context, req *http.Request, resp *fhttp.Response, fn func(ctx context.Context, resp *fhttp.Response) error) error
}

type idempotencyMiddleWare struct {
	tx   sql.Transactional
	repo domain.IdempotencyRepository
}

// TODO check
//Idempotency - Middleware to add Idempotency-Key to context
func (i *idempotencyMiddleWare) Idempotency(ctx context.Context, req *http.Request, resp *fhttp.Response, fn func(ctx context.Context, resp *fhttp.Response) error) error {
	// check are client enclose idempotency-key in header

	idempotencyKeyID := req.Header.Get(constants.IdempotencyKey.String())
	if idempotencyKeyID == "" {
		// TEST --> send request without idempotency-key in header
		return errors.ErrKindInternalServerError(errors.ErrCodeIdempotencyMissing, "Please provide Idempotency-Key in request header")
	}

	// convert request body to string before insert to db
	// TODO req.Body is missing
	// TODO We need marshall
	requestBody := requestBodyToString(ctx, req.Body)

	// check if data is exists / can processable
	record, err := i.IsRequestProcessed(ctx, req.Method, req.URL.Path, requestBody, idempotencyKeyID)
	if err != nil {
		return errors.ErrKindInternalServerError(errors.ErrCodeInternalServerError, "Failed to check idempotency")
	}

	/*
		if responseCode is non zero then responseBody.String has possible values
			1. ""
			2. non-empty data to return

		response data or response status might be incorrect(if code has bug
		and saved after service layer execution) but prevent to run service layer again for
		same idempotency key.
	*/

	// check are ResponseCode is exists in db
	if record.ResponseCode.Valid {
		logger.Info(ctx, "Returning response from idempotency check. Data %v.", idempotencyKeyID)
		resp.StatusCode = int(record.ResponseCode.Int64)
		if err = json.Unmarshal([]byte(record.ResponseBody.String), &resp.Data); err != nil {
			return errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeInvalidUnmarshall, err, "Response ResponseCode")
		}
		return nil
	}

	//Add Idempotency-Key to context
	ctx = context.WithValue(req.Context(), constants.IdempotencyKey, idempotencyKeyID)
	//Add IncomingRequestID
	IncomingRequestID := "Incoming-Request-ID"
	ctx = context.WithValue(ctx, IncomingRequestID, idempotencyKeyID)
	ctx = logger.AddValueToContext(ctx, string(constants.IdempotencyKey), idempotencyKeyID)
	logger.Info(ctx, "Idempotency-Key: %v", idempotencyKeyID)

	// create trx for update idempotency
	if err = i.tx.WithTransaction(ctx, func(ctx context.Context) error {
		// create response
		if err := fn(ctx, resp); err != nil {
			return errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeInternalServerError, err, "Create Response")
		}

		// marshalling response data
		respData, err := json.Marshal(resp.Data)
		if err != nil {
			return errors.ErrKindUnprocessableWithCause(errors.ErrCodeInvalidMarshall, err, "Response Data")
		}

		// set response based on data
		record.SetResponse(resp.StatusCode, string(respData))

		// update record data on rb based on record
		err = i.repo.Update(ctx, record)
		if err != nil {
			return errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "Update Idempotency")
		}

		return nil
	}); err != nil {
		return errors.ErrKindInternalServerErrorWithCause(errors.ErrInternalCode, err, "Failed in idempotency")
	}
	return nil
}

func (i *idempotencyMiddleWare) IsRequestProcessed(ctx context.Context, requestMethod string, requestPath string, requetsParam string, idempotencyKeyID string) (*domain.Idempotency, error) {
	// check are data idempotency is exists in db with specific parameter
	idempotencyKey, err := i.repo.FindByKeyAndRequestMethodAndRequestPath(ctx, idempotencyKeyID, requestMethod, requestPath)
	if err != nil {
		return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "FindByKeyAndRequestMethodAndRequestPath")
	}

	// if idempotency is not null
	if idempotencyKey != nil {
		// make sure data response body is exists
		if idempotencyKey.ResponseBody.Valid {
			return idempotencyKey, nil
		}

		// make sure data LockedAt is not null AND time of locket at is less than 5 minutes since create
		if idempotencyKey.LockedAt != nil && time.Since(*idempotencyKey.LockedAt).Seconds() < IdempotencyKeyLockTimeout {
			errMsg := fmt.Sprintf("Request is already in progress for idempotency key %s.", idempotencyKeyID)
			logger.Error(ctx, errMsg)
			return nil, ferror.E(errMsg)
		}

		// if locked at is null, create new locked at with current time
		*idempotencyKey.LockedAt = time.Now().UTC()

		// update data
		err = i.repo.Update(ctx, idempotencyKey)
		if err != nil {
			return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "Update Idempotency")
		}
	} else {
		// if idempotency is nil
		idempotencyKey = &domain.Idempotency{}

		// create new request data
		idempotencyKey.SetRequest(idempotencyKeyID, requestPath, requestMethod, requetsParam)

		// save data
		id, err := i.repo.Save(ctx, idempotencyKey)
		if err != nil {
			return nil, errors.ErrKindInternalServerErrorWithCause(errors.ErrCodeRepositoryError, err, "Save Idempotency")
		}

		// set idempotency key id
		idempotencyKey.ID = id
	}

	return idempotencyKey, nil
}

func requestBodyToString(ctx context.Context, requestBody io.ReadCloser) string {
	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(requestBody); err != nil {
		logger.Error(ctx, ferror.StackTrace(err))
	}
	return buf.String()
}

func marshalResponse(response string) ([]byte, error) {
	res, err := json.Marshal(response)
	if err != nil {
		return nil, ferror.E(fmt.Sprintf("Error parsing response body: %v", err.Error()))
	}
	return res, nil
}

func NewIdempotencyMiddleWare(tx sql.Transactional, repo domain.IdempotencyRepository) (IdempotencyMiddleWare, error) {
	return &idempotencyMiddleWare{
		tx:   tx,
		repo: repo,
	}, nil
}
