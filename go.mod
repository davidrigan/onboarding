module bitbucket.org/finaccelteam/onboarding

go 1.17

require (
	bitbucket.org/finaccelteam/ms-common-go/broker v1.4.5
	bitbucket.org/finaccelteam/ms-common-go/config v0.1.5
	bitbucket.org/finaccelteam/ms-common-go/controller v0.1.1
	bitbucket.org/finaccelteam/ms-common-go/ferror v1.0.5
	bitbucket.org/finaccelteam/ms-common-go/ferror/v2 v2.0.2-temp-2
	bitbucket.org/finaccelteam/ms-common-go/fhttp v0.2.0
	bitbucket.org/finaccelteam/ms-common-go/logger v1.1.1
	bitbucket.org/finaccelteam/ms-common-go/sql v1.2.3
	bitbucket.org/finaccelteam/ms-common-go/storage v0.2.0
	bitbucket.org/finaccelteam/ms-common-go/types v0.1.1
	bitbucket.org/finaccelteam/ms-common-go/util v1.2.3
	github.com/go-playground/validator/v10 v10.4.1
	github.com/google/uuid v1.1.2
	github.com/google/wire v0.5.0
	github.com/gorilla/mux v1.8.0
	github.com/shopspring/decimal v1.3.1
	github.com/stretchr/testify v1.7.0
)

require (
	bitbucket.org/finaccelteam/ms-common-go/errors v0.1.0 // indirect
	bitbucket.org/finaccelteam/ms-common-go/response v0.1.0 // indirect
	bitbucket.org/finaccelteam/ms-common-go/slack v1.0.2 // indirect
	github.com/armon/go-radix v1.0.0 // indirect
	github.com/confluentinc/confluent-kafka-go v1.5.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/elastic/go-sysinfo v1.1.1 // indirect
	github.com/elastic/go-windows v1.0.0 // indirect
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/go-playground/locales v0.13.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/joeshaw/multierror v0.0.0-20140124173710-69b34d4ec901 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/lib/pq v1.3.0 // indirect
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/pelletier/go-toml v1.2.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/procfs v0.0.3 // indirect
	github.com/santhosh-tekuri/jsonschema v1.2.4 // indirect
	github.com/slack-go/slack v0.8.2 // indirect
	github.com/spf13/afero v1.1.2 // indirect
	github.com/spf13/cast v1.3.0 // indirect
	github.com/spf13/jwalterweatherman v1.0.0 // indirect
	github.com/spf13/pflag v1.0.3 // indirect
	github.com/spf13/viper v1.6.2 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/subosito/gotenv v1.2.0 // indirect
	go.elastic.co/apm v1.11.0 // indirect
	go.elastic.co/apm/module/apmsql v1.11.0 // indirect
	go.elastic.co/fastjson v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.5.2 // indirect
	gopkg.in/ini.v1 v1.51.0 // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
	howett.net/plist v0.0.0-20181124034731-591f970eefbb // indirect
)
