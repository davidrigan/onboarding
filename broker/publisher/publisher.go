package publisher

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/broker/kafka"
	commonBroker "bitbucket.org/finaccelteam/onboarding/broker"
)

var kafkaPublisher broker.Publisher

// kafka publish config - Three
func GetKafkaEventPublisher() (broker.Publisher, error) {
	if kafkaPublisher == nil {
		config := commonBroker.GetKafkaConfig()
		var disableSSLVerificationOption kafka.PublisherOption
		if config.DisableSSLVerification != nil {
			disableSSLVerificationOption = kafka.PublisherDisableSSLVerificationOption(config.DisableSSLVerification)
		}
		var awk kafka.PublisherOption
		awk = kafka.PublisherAwkOption(config.Awk)
		return kafka.NewPublisher(config.BootstrapServer, disableSSLVerificationOption, awk)
	}
	return kafkaPublisher, nil
}
